## IOOpt: Compute parametric I/O cost and optimize tiling loops permutation and sizes for perfectly nested affine code

# Installation with docker

Start by cloning the git repository:
```bash
	git clone https://gitlab.inria.fr/corse/ioopt.git
```

To set up the docker container:

```bash
	docker login registry.gitlab.inria.fr ## You may have to be sudo to execute this command
	docker pull registry.gitlab.inria.fr/corse/ioopt ## sudo
```

To start the container:
```bash
	cd ioopt
	docker run -ti --rm  -v$(pwd):/hst -w/hst registry.gitlab.inria.fr/corse/ioopt bash ## sudo 
```

To avoid using sudo to run docker you can check: https://docs.docker.com/engine/install/linux-postinstall/.

This shares the current directory with the container. 
You can then run ioopt.py within the container as follow:
```bash
   ./ioopt.py gemm.yml -s 
```
Alternatively you can use ioopt-indocker bash script to launch docker for you, run ioopt.py, and quit as follow:
```bash
   ./ioopt-indocker gemm.yml -s
```

If you use the integral solver (option -i), you need to add the binary "couenne" to the ioopt folder (available here: https://ampl.com/products/solvers/open-source-solvers/#couenne or here https://github.com/coin-or/Couenne )


# Usage
As detailed below, ioopt uses a yaml file for describing the problem. 
The current version is restricted to perfectly nested loops with rectangular shapes. 
Also, reuse directions have to be given by the user (see below).
For a given program, ioopt prunes amoung all possible loop permutations duplicate one that would have the same I/O cost.
With option `--cost`, ioopt dumps all those permutations, the associated data footprint and cost for each cache level.
The overall memory depth can be set to less than the one specified in the yaml file using the `--depth` option:
```bash
   ./ioopt.py --cost gemm.yml --depth 1
```

Among all those permutations, for a given problem size (specified in the parameters section in the yaml file -- see below), one minimizes the cost function. 
With option `--solve` ioopt uses ipopt to solve this problem for you.
The solution is a loop nest where loops reduced to one iteration can be removed (leading to a permutation with less loops).

```bash
  ./ioopt-indocker --solve gemm.yml --depth 1
```

Option `--schedule` can be used to dump the corresponding loop nest is C-like format.

Finally, it is possible to force the size of the innermost dimensions (called micro-kernel). 
For example, the scheme used in BLIS for gemm can be automatically obtained as follow:
```bash
  ./ioopt-indocker --solve gemm.yml --microkernel i=6 j=2
```

# Generating and running CNN and TC examples

To generate all tests in directory `tests/`:
```bash
   python gen_tests.py
```

To run IOOpt on a test (for example `Yolo9000-0.yml`):
```bash
   python ioopt.py tests/Yolo9000-0.yml -s -o Yolo9000-0.out
```

This prints information on the optimization problem solving on stdout, and generates a tiling scheme in `Yolo9000-0.out`.


# Input format details: matrix multiplication example

Let us explain how yaml input files are derived from actual codes, using
matrix multiplication.

```C
for(int i = 0; i < Ni; i++)
	for(int j = 0; j < Nj; j++)
		for(int k = 0; k < Nk; k++)
			C[i][j] += A[i][k] * B[k][j];
```

The first step is to check that the code is fully permutable and tileable.
This can be done manually using PluTo (TODO).
Here no transformation is needed:
the code already consists of fully permutable, rectangularly tileable loops.

Let us explain in detail the corresponding yaml input file:

```yaml
dims: [i, j, k]

arrays:
   A: [[i, k], 1]
   B: [[k, j], 1]
   C: [[i, j], 2]

reuse:
   A: [j]
   B: [i]
   C: [k]

values:
  n_levels: 3
  cache: [8192, 262144, 8388608]
  bw: [8, 8, 0.75]
  dims: [1000, 1000, 1000]
  microkernel: {i : 32, j : 12}

```

`dims` contains the loop index variables. This is straightforward here, there are three
loops indexed `i, j, k`.

`arrays` describes how arrays are accessed inside a tile.
It is a dictionnary mapping array names of type `str` to a pair of type `(List[str], int)`.
The first element of the pair describes the array access function, where each array subscript
can be an arbitrary affine function of loop dimensions.
The second element is a multiplier: here we count 1 for reads and 2 for read/writes.

`reuse` describes the reuse dimensions: basically a dimension which is not listed in
the access function of an array is a reuse dimension for that array.

`values` contains the numerical values for the non-linear optimization.
Here this describes a 3-level cache. Cache capacities are given in words (1 word = 8 bytes on
a 64-bit architecture). `bw` is the cache bandwidth for each level. `dims` is
the dimension range for each dimension (here this mean `Ni = Nj = Nk = 1000`).
Finally, `microkernel` optionnaly describes a microkernel, i.e. a manual loop permutation
scheme for the innermost level, manually tuned to use vectorization and register tiling.
Dimension are given innermost to outermost, as a dictionary mapping dimensions to values.

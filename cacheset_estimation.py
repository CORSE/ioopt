
import math


# Given a access function (linearized, in a rectangular array), a rectangular tile
#	and a number of cache sets,
#   this script computes the pattern of cache line distribution across cache sets,
#	in the "steady-state" period.


_print_info = False

# Toggle to use iscc instead of the isl lib Python interface
_iscc_usage = False

if _iscc_usage:
	# [Deprecated] ./iscc interface (CHANGE THAT IF YOU RUN THIS SCRIPT ON ANOTHER MACHINE)
	import subprocess
	iscc_bin_path = "/home/giooss/lib/barvinok/build/bin/iscc"
else:
	import isl



# [Deprecated] Interfacing with iscc (to use the card)
def iscc_call(str_command):

	# We create a temporary file containing str_command
	name_input_file = "__temp_iscc_input.txt"
	fin = open(name_input_file, 'w')
	fin.write(str_command)
	fin.close()

	p = subprocess.Popen([iscc_bin_path], stdout=subprocess.PIPE,
			stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
	b_str_command = bytes(str_command, 'utf-8')
	proc_output = p.communicate(input=b_str_command)[0]
	str_output = proc_output.decode()

	return str_output

# [Deprecated] Parsing the output of iscc. Should be of the form "{ [int] }"
def card_parsing(str_output_iscc):
	str_val = str_output_iscc.splitlines()[0].strip("{} ")
	#print(f"str_val = \"{str_val}\"")

	if str_val == "":
		int_val = 0
	else:
		int_val = int(str_val)
	return int_val


# [Deprecated] Debug function for the iscc calls
def debug_iscc_call():
	str_command = "D := { [i,j,k] : exists l : 6k+10j+i=64l+0 and 0<=j<4 and 0<=i<5 and 0<=k<160 };"
	str_command = str_command + "\ncard(D);"
	str_output = iscc_call(str_command)
	#print(str_output)

	int_card = card_parsing(str_output)
	print(int_card)
	# END DEBUG

	assert(int_card == 60)
	return


# New way to call isl, for the Ioopt lib
def isl_card_computation(str_set):
	isl_idx_set = isl.union_set(str_set)
	card_str = str(isl_idx_set.card())
	return card_parsing(card_str)


# ========================================================================

def lcm(lcoeff):
	assert(lcoeff != [])

	lcm = lcoeff[0]
	for i in range(1, len(lcoeff)):
		temp = math.gcd(lcm, lcoeff[i])
		lcm = int(lcm * lcoeff[i] / temp)

	return lcm

#print(lcm([2,3,4]))


# Assuming a unbounded i1, compute the period of pattern
def compute_period_pattern(access_func_coeff, Nset, bound_tiles):
	# Nboucle
	Nboucle = lcm([Nset, access_func_coeff[0]])
	i1loop = int(Nboucle / access_func_coeff[0])


	# Period on i1 to have a periodic pattern
	per = math.gcd(Nset, access_func_coeff[0])
	#for k in range(len(access_func_coeff)-1):
	#	per = math.gcd(per, access_func_coeff[k])
	#print(per)

	# For each phase of this period, get the number of 
	period_pattern = []
	for k in range(per):
		# Exemple:
		#str_command_k = "D := { [i,j,k] : exists l : 6k+10j+i=64l+0 and 0<=j<4 and 0<=i<5 and 0<=k<160 };"
		#str_command_k = str_command_k + "\ncard(D);"

		str_set_k = "{ ["
		# Indices
		for x in range(len(access_func_coeff)):
			str_set_k = str_set_k + "i" + str(x)
			if x<len(access_func_coeff)-1:
				str_set_k = str_set_k +","
		str_set_k = str_set_k + "] : exists l : "

		# Equality constraint
		for x in range(len(access_func_coeff)):
			if x>0:
				str_set_k = str_set_k + "+"
			str_set_k = str_set_k + str(access_func_coeff[x]) + "i" + str(x)
		str_set_k = str_set_k + "=64l+" + str(k)
		
		# Constraints on the ik, k>x0
		for x in range(1,len(bound_tiles)):
			lubound = bound_tiles[x]
			str_set_k = str_set_k + " and "	+ str(lubound[0]) + "<=i"
			str_set_k = str_set_k + str(x) + "<" + str(lubound[1])

		# Constraint for i0
		str_set_k = str_set_k + " and 0<=i0<" + str(i1loop)
		str_set_k = str_set_k + " }"

		# DEBUG
		#print(str_set_k)

		if _iscc_usage:
			str_command_k = "D := " + str_set_k + ";\ncard(D);"
			str_output_iscc = iscc_call(str_command_k)
			cardk = card_parsing(str_output_iscc)
		else:
			cardk = isl_card_computation(str_set_k)

		# DEBUG
		#print(cardk)

		period_pattern.append(cardk)

	assert(len(period_pattern) == per)
	return period_pattern



# ========================================================================


# Given a list "period_pattern" describing a periodic cache line distribution over cache sets
#		estimates the percentage of cache that is actually used by this
#		access when we start spilling
def estimate_cache_perc_util(period_pattern):
	per = len(period_pattern)

	# Saturating sets
	cmax = max(period_pattern)

	acc_per_cacheset = 0.0
	for c in period_pattern:
		acc_per_cacheset += (cmax - c) / (cmax * per)
	perc_util = 1 - acc_per_cacheset

	frac_perc_util = []
	frac_perc_util.append(int(per * cmax * perc_util))
	frac_perc_util.append(cmax * per)

	# Small optim
	elim = math.gcd(frac_perc_util[0], frac_perc_util[1])
	frac_perc_util[0] = frac_perc_util[0] / elim
	frac_perc_util[1] = frac_perc_util[1] / elim


	#print(perc_util)
	#print(frac_perc_util)

	# Return it as either a float, or a fraction (list of 2 elems, better for problem encoding)
	return (perc_util, frac_perc_util)


# Debug function for the percentage estimator
def debug_estimate_cache_perc_util():
	period_pattern = [6, 4]
	perc_util, frac_perc_util = estimate_cache_perc_util(period_pattern)
	print(perc_util)
	# => 0.8333333333333334   (= 5/6)
	print(frac_perc_util)

	return



# ========================================================================


# Check if the provided inputs are well-built
def check_input(access_func_coeff, bound_tiles):
	# Check that a_{k+1} divides a_{k}
	for k in range(len(access_func_coeff)-1):
		akp1 = access_func_coeff[k+1]
		ak = access_func_coeff[k]

		if (ak % akp1)!=0:
			print(f"Error - access function coefficient at {k+1} ({akp1}) does not divide {ak}.")
			exit(0)

	# Check the range of the tile: sum_{l=k+1}^{N} a_l * i_l \in [0, a_k]
	for k in range(len(access_func_coeff)):
		# Same coefficient: dim are at the same level (conv like) => skip the check
		if k<len(access_func_coeff)-1:
			if (access_func_coeff[k] == access_func_coeff[k+1]):
				continue

		lk = bound_tiles[k][0]
		uk = bound_tiles[k][1]

		if (lk<0):
			print(f"Error - lower bound at dim {k} ({lk}) cannot be non-positive.")
			exit(0)

		acc_sum = 0
		for l in range(k+1, len(access_func_coeff)):
			al = access_func_coeff[l]
			ul = bound_tiles[l][1]
			acc_sum += al * (ul-1)
		if acc_sum >= access_func_coeff[k]:
			# DEBUG
			#print(acc_sum)
			#print(access_func_coeff[k])
			print(f"Error - upper bound at dim {k} ({uk}) is too low.")
			exit(0)

	return


# Preprocess the input to simplify the problem (and the algorithm)
def preprocessing(access_func_coeff, Nset, bound_tiles):
	# If Lk = UK-1, then remove the dim from the contribution
	# TODO: note - the constant is not tracked here (...yet?)
	k=0
	while k<len(bound_tiles):
		luk = bound_tiles[k]
		if luk[1] - luk[0] == 1:
			# Remove the kth dim from bound_tiles and access_func_coeff
			access_func_coeff = access_func_coeff[:k] + access_func_coeff[k+1:]
			bound_tiles = bound_tiles[:k] + bound_tiles[k+1:]
		k+=1

	# Modulo Nset
	for k in range(len(access_func_coeff)):
		access_func_coeff[k] = access_func_coeff[k] % Nset

	# Coeff at 0
	k0max = -1
	for k in range(len(access_func_coeff)):
		if (access_func_coeff[k]==0):
			k0max = k
	if k0max!=(-1):
		# Because a_{k+1} divides a_{k}, for all k
		for l in range(k0max):
			assert(access_func_coeff[l]==0)

		access_func_coeff = access_func_coeff[k0max+1:]
		bound_tiles = bound_tiles[k0max+1:]

	assert(len(access_func_coeff) == len(bound_tiles))

	return (access_func_coeff, bound_tiles)



# Main entry function for the algorithm
#	access_func_coeff = list of integral coefficient (no constant term) for dims ik
#	Nset = integer (number of cache sets)
#	bound_tiles = list of (list of 2 elements: lk, uk) such that lk<=ik<uk
def main_algo_fun(access_func_coeff, Nset, bound_tiles):

	# DEBUG - Input
	#print(f"access_func_coeff = {access_func_coeff}")
	#print(f"Nset = {Nset}")
	#print(f"bound_tiles = {bound_tiles}")

	assert(len(access_func_coeff) == len(bound_tiles))
	check_input(access_func_coeff, bound_tiles)

	(access_func_coeff, bound_tiles) = preprocessing(access_func_coeff, Nset, bound_tiles)

	if len(access_func_coeff)==1:
		lub = bound_tiles[0]
		perc_util = (lub[1] - lub[0]) / Nset

		if perc_util>1:
			# Note: we do not have periodicity issue, so this (commented) portion is wrong...
			"""
			mod = (lub[1] - lub[0]) % Nset
			div = int( ((lub[1] - lub[0]) - mod) / Nset )
			# All cache sets have div cache lines, except mod that has (div+1)
			# (Nset-mod) cache lines are missing to have full equilibrum
			acc_per_cacheset = (Nset-mod) / ((div+1) * Nset)
			perc_util = 1 - acc_per_cacheset
			"""
			perc_util = 1

		if _print_info:
			print(f"We only have a single interval here: [{lub[0]}, {lub[1]}[.")
			print(f"\tThe cache set occupancy is {perc_util*100} % of the cache sets.")

		return perc_util, [1,1]

	period_pattern = compute_period_pattern(access_func_coeff, Nset, bound_tiles)

	# First output of the algorithm
	if _print_info:
		print(f"period_pattern = {period_pattern}^*")

	(perc_util, frac_perc_util) = estimate_cache_perc_util(period_pattern)

	# Second output of the algorithm
	if _print_info:
		print(f"This access function and this tile can only use {perc_util*100} % of the cache")
		print("\tat best, when moving the size along the outermost dimension of the access.")

	return (perc_util, frac_perc_util)


# ========================================================================

# For debugging
def main_debug():
	#debug_iscc_call()
	#debug_estimate_cache_perc_util()

	# Inputs
	test_case = 1

	if test_case==1:
		access_func_coeff = [60, 10, 1] # 60k+10j+i
		Nset = 64						# Number of cache sets
		bound_tiles = [
			[0, 20],	# 0 <= k < 20
			[0, 4],		# 0 <= j < 4
			[0, 5]		# 0 <= i < 5
		]
	elif test_case==2:
		access_func_coeff = [32, 8, 1] # 32k+8j+i
		Nset = 64						# Number of cache sets
		bound_tiles = [
			[0, 20],	# 0 <= k < 20
			[0, 2],		# 0 <= j < 2
			[0, 5]		# 0 <= i < 5
		]
	elif test_case==3:
		access_func_coeff = [128, 64, 1] # 128k+64j+i
		Nset = 64						# Number of cache sets
		bound_tiles = [
			[0, 20],	# 0 <= k < 20
			[0, 2],		# 0 <= j < 2
			[0, 5]		# 0 <= i < 5
		]
	elif test_case==4:
		access_func_coeff = [256, 128, 1] # 256k+128j+i
		Nset = 64						# Number of cache sets
		bound_tiles = [
			[0, 20],	# 0 <= k < 20
			[0, 2],		# 0 <= j < 2
			[0, 67]		# 0 <= i < 67
		]
	else:
		print("Test case number not recognized!")
		exit(0)


	""" # Example on the whiteboard (Yolo9000-09, Param access)
	access_func_coeff = [8, 1]
	Nset = 64
	bound_tiles = [
		[0, 64],
		[0, 4],
	]
	"""
	
	access_func_coeff = [60, 60, 10, 1]
	Nset = 64
	bound_tiles = [
		[0, 10],
		[0, 3],
		[0, 4],
		[0, 3],
	]

	main_algo_fun(access_func_coeff, Nset, bound_tiles)
	return


# Launch!
if __name__ == '__main__':
    main_debug()



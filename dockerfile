FROM ubuntu:latest
RUN <<EOF
apt update 
apt install -y \
	git build-essential autoconf libtool pkg-config \
        llvm libgmp-dev libyaml-dev clang libclang-dev libclang-cpp-dev 
EOF

RUN <<EOF
# ntl
cd /tmp
git clone https://github.com/libntl/ntl.git
cd /tmp/ntl/src
./configure NTL_GMP_LIP=on PREFIX=/opt/ntl GMP_PREFIX=/usr/local SHARED=on
make
make install
EOF

RUN <<EOF
# barvinok
cd /tmp
git clone git://repo.or.cz/barvinok.git
cd barvinok
./get_submodules.sh
sh autogen.sh
./configure --prefix=/opt/barvinok --with-ntl-prefix=/opt/ntl --with-pet=bundled --with-gmp-prefix=/usr/local/lib --with-clang=system --enable-shared-barvinok
make
make isl.py
make install
EOF

# python modules
RUN apt install -y pip
RUN pip3 install --break-system-packages sympy memoization amplpy numpy

# ampl
RUN apt install -y wget
RUN cd /tmp && wget https://portal.ampl.com/dl/amplce/amplbundle.linux64.tgz
RUN cd /opt && tar xf /tmp/amplbundle.linux64.tgz
ENV PATH=$PATH:/opt/ampl.linux-intel64/
RUN amplkey activate --uuid 070e550f-8437-4c42-8adc-de80297f9190


# paths 
ENV PATH=$PATH:/opt/barvinok/include 
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/barvinok/lib 
ENV PYTHONPATH=$PYTHONPATH:/home/giooss/lib/barvinok

# ==========================================================================
#    Copyright (C) INRIA
#    Contributors: Auguste Olivry
#    Date of creation: 2020 - 2021
#
#    Emails: auguste.olivry@inria.fr
#
#    This software is a computer program whose purpose is to derive
#    automatically a lower-bound to the IO-complexity of a polyhedral
#    program.
#
#    This software is governed by the CeCILL  license under French law and
#    abiding by the rules of distribution of free software.  You can  use, 
#    modify and/ or redistribute the software under the terms of the CeCILL
#    license as circulated by CEA, CNRS and INRIA at the following URL
#    "http://www.cecill.info". 
#
#    As a counterpart to the access to the source code and  rights to copy,
#    modify and redistribute granted by the license, users are provided only
#    with a limited warranty  and the software's author,  the holder of the
#    economic rights,  and the successive licensors  have only  limited
#    liability. 
#
#    In this respect, the user's attention is drawn to the risks associated
#    with loading,  using,  modifying and/or developing or reproducing the
#    software by the user in light of its specific status of free software,
#    that may mean  that it is complicated to manipulate,  and  that  also
#    therefore means  that it is reserved for developers  and  experienced
#    professionals having in-depth computer knowledge. Users are therefore
#    encouraged to load and test the software's suitability as regards their
#    requirements in conditions enabling the security of their systems and/or 
#    data to be ensured and,  more generally, to use and operate it in the 
#    same conditions as regards security. 
#
#    The fact that you are presently reading this means that you have had
#    knowledge of the CeCILL license and that you accept its terms.
# ==========================================================================

import yaml
import sys
import os

try:
    os.mkdir('./tests/')
except FileExistsError:
    pass

def gen_cnn():
    with open('CNN.txt') as fcnn:
        y = yaml.load(fcnn.read(), Loader=yaml.SafeLoader)
        for test_name, test_case in y.items():
            stride_str = ''
            if 'stride' in test_case:
                stride_str = f'{test_case["stride"]} * '
            f, c, x, y, w, h = test_case['K*C*H*W*R*S'].split('*')

            with open(f'tests/{test_name}.yml', 'w') as ftest:
                ftest.write(f'''\
    dims : [f, c, x, y, w, h]

    arrays:
       O : [ [f, x, y], 2]
       I : [ [c, {stride_str}x + w, {stride_str}y + h], 1]
       K : [ [f, c, w, h], 1]
    reuse : 
        O: [c, w, h]
        I: [f, w, h]
        K: [x, y]

    values:
      n_levels: 3
      cache: [8192, 262144, 8388608]
      bw: [8, 8, 0.75]
      dims: [{f}, {c}, {x}, {y}, {w}, {h}]
      microkernel: {{f : 32, y : 12}}
    ''')

def gen_tc():
    with open('TC.txt') as fcnn:
        y = yaml.load(fcnn.read(), Loader=yaml.SafeLoader)
        for test_name, dims in y.items():

            with open(f'tests/{test_name}.yml', 'w') as ftest:
                dc, da, db = map(list, test_name.split('-'))
                ra, rb, rc = map(lambda l : list(set(dims.keys()) - set(l)), (da, db, dc))
                ftest.write(f'''\
    dims: {list(dims.keys())}

    arrays:
        A : [ {da}, 1]
        B : [ {db}, 1]
        C : [ {dc}, 1]
    reuse:
        A : {ra}
        B : {rb}
        C : {rc}

    values:
        n_levels: 3
        cache: [8192, 262144, 8388608]
        bw: [8, 8, 0.75]
        dims: {list(dims.values())}
        microkernel: {{ {dc[0]} : 32, {dc[1]} : 12 }}
    ''')

gen_cnn()
gen_tc()

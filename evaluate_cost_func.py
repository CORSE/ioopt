
import ioopt


# Parse a string that corresponds to a Ttile scheme, and transform it
#	into a list of triplets (ex: ["T", "x", 42] , ["U", "f", 2], ["V", "f", 8]
# Note: put dimensions in lowercase to match the dims of the Ioopt file
#
# Example of scheme:
#   [V F; (* U (1, F); *) U (3, H); U(7, Y);  T (256, C);  Hoist_vars [C];
#      T(7, X); T(3, W); T(4, C); T(128, F);
#      (*Lambda_apply (C, [(Iter 4, Arg 240); (Iter 1, Arg 64)]); *)
#   ]
# TODO: Not managed (yet?): Packing / Lambda / R (would require problem sizes?)
def parse_ttile_scheme(str_scheme, vector_len):
	scheme = []

	# Remove the leading/finishing empty chars
	str_scheme = str_scheme.lstrip()

	# Assume first and last char are [ and ]
	assert(str_scheme[0]=="[")
	assert(str_scheme[-1]=="]")
	str_scheme = str_scheme[1:-1]

	# Consider each atom one by one
	latoms = str_scheme.split(";")

	# Aux function, parse atoms of the shape "U(d, 42)" or "T(d, 20)"
	def parse_T_U_atom(str_atom):
		# Remove parenthesis
		lstr_atom = str_atom.split("(")
		assert(len(lstr_atom)==2)

		lstr_atom = lstr_atom[1].split(")")
		assert(len(lstr_atom)<=2)

		lstr_atom = lstr_atom[0].split(",")
		size = int(lstr_atom[0].lstrip())
		dim = lstr_atom[1].lstrip().lower()

		return (size, dim)

	for str_atom in latoms:
		str_atom = str_atom.lstrip()

		if (str_atom[0] == "V"):
			lstr_atom = str_atom.split(" ")
			assert(len(lstr_atom)==2)
			dim = lstr_atom[1].lstrip().lower()

			triple_ret = ("V", dim, vector_len)
			scheme.append(triple_ret)

		elif (str_atom[0] == "U"):
			size, dim = parse_T_U_atom(str_atom)
			triple_ret = ("U", dim, size)
			scheme.append(triple_ret)

		elif (str_atom.startswith("Tile_partial") or
				str_atom.startswith("Tile_gexact") or
				str_atom.startswith("Tile_exact")):
			# Tile_partial (48, Y)
			size,dim = parse_T_U_atom(str_atom)
			triple_ret = ("Tpart", dim, size)
			scheme.append(triple_ret)

		elif (str_atom[0] == "T"):
			size, dim = parse_T_U_atom(str_atom)
			triple_ret = ("T", dim, size)
			scheme.append(triple_ret)

		elif (str_atom.startswith("Hoist_vars")):
			# We ignore the Hoist_vars atom
			continue
		else:
			print(f"ERROR: parse_ttile_scheme - atom not supported: {str_atom}")
			assert(False)

	return scheme


# Given d_sizes the dimensions of a rectangular tile size,
#	compute the footprint of each array (specified in dict_arrays)
# NOTE: footprint is in number of elements (ex: float)
def compute_footprint(dict_arrays, d_sizes):
	# d_sizes keys are dims, that must match the dims used in
	#	the expressions on the right of dict_arrays

	dfootprint = dict()
	for (a, lexpr_sizes) in dict_arrays.items():
		fp = 1 #size_float
		for expr_sizes in lexpr_sizes[0]:
			val_size = eval(expr_sizes, d_sizes)
			fp *= val_size
		dfootprint[a] = fp

	return dfootprint


# Split scheme in levels (according to its footprints and the cache sizes)
def split_scheme_into_level(scheme, prog, lcachesizes):
	# => llsplit_scheme
	nlevels = len(lcachesizes)
	cur_level = 0
	llsplit_scheme = [ [] ]

	# DEBUG
	#print(lcachesizes)
	#print(nlevels)

	dict_arrays = prog.arrays
	d_sizes = dict()
	for d in prog.dims:
		d_sizes[d] = 1

	for atom in scheme:
		# Update d_sizes
		if atom[0]== "Tpart":
			# atom[2] is a tile size
			d_sizes[atom[1]] = atom[2]
		else:
			# atom[2] is a ratio
			d_sizes[atom[1]] = d_sizes[atom[1]] * atom[2]

		# Compute the new footprint
		dfp = compute_footprint(dict_arrays, d_sizes)

		# Check if we have exceeded the current cache level
		sum_fp = 0
		for fp in dfp.values():
			sum_fp += fp

		# DEBUG
		if False:
			if sum_fp<1024:
				print(f"Atom {atom} - Total footprint = {sum_fp} float")
			elif sum_fp < 1048576:
				print(f"Atom {atom} - Total footprint = {sum_fp/1024} Kfloat")
			else:
				print(f"Atom {atom} - Total footprint = {sum_fp/1048576} Mfloat")


		# RAM-level case
		if (cur_level>=nlevels):
			llsplit_scheme[-1].append(atom)
			continue

		if (sum_fp > lcachesizes[cur_level]):
			cur_level += 1
			llsplit_scheme.append([])
			llsplit_scheme[-1].append(atom)
		else:
			llsplit_scheme[-1].append(atom)

	# Complete llsplit_scheme if missing levels
	for i in range(len(llsplit_scheme), nlevels+1):
		llsplit_scheme.append([])

	return llsplit_scheme


# Compute the size of the current tile of the scheme
def get_sizes_scheme(scheme, ldims):
	d_sizes = dict()
	for d in ldims:
		d_sizes[d] = 1

	for atom in scheme:
		if atom[0]=="Tpart":
			d_sizes[atom[1]] = atom[2]
		else:
			d_sizes[atom[1]] = d_sizes[atom[1]] * atom[2]

	return d_sizes


# ===================================================

# Print-out the values of the different cost_func for the scheme found in sol
def print_cost_func_sol_info(dcost_func, param_values, sol):
	# sol: (s, v) where:
	# 	s (dict): X (variable to be solved) --> value of X  (converted to integer later)
    # 	v: value of the objective function
	
	# New line
	print()

	# Separate costs
	i = 0
	for (lvl, cost_func_lvl) in dcost_func.items():
		dsol = dict()
		dsol = dsol | sol[0]  # Add the entries from the dict sol[0]
		dsol = dsol | param_values
		#print(dsol)

		val_cost_func_lvl = eval(str(cost_func_lvl), dsol)
		print(f"* Level {lvl} - cost_func = {int(val_cost_func_lvl)}")
		print(f"\t (cost function = {str(cost_func_lvl)} )")
		i += 1
	# Levels above
	#for k in range(i, len(llsplit_scheme)):
	#	print(f"* Above (scheme part: {llsplit_scheme[k]})")

	# Global cost
	print(f"=> Global cost = {int(sol[1])}")
	return


# Taking the cost function of the solution (corresponding to the problem selected)
#	apply the scheme to it
def print_cost_func_scheme_info(llsplit_scheme, ldims, combined_cost_func, dcost_func, param_values):
	# New line
	print()

	# 1) Separate costs
	i = 0
	inner_scheme = [] # Inner part of the scheme (up to level i)
	for (lvl, cost_func_lvl) in dcost_func.items():
		# Add the current level of the scheme to the scheme
		inner_scheme = inner_scheme + llsplit_scheme[i]

		dsol = dict()
		dsol = dsol | param_values
		
		dsizes_scheme = get_sizes_scheme(inner_scheme, ldims)

		# Example of names for tilevar_name : Ty_0 / Th_1
		# Guaranty that a dim will be used only one in the cost function
		#  (ex: Ty_0 and Ty_1 cannot happens in the same cost fun)
		#	=> Because we cannot know if it is a 0 or 1 (reuse or not of
		#		previous tile size), we assign both the same value.
		dsizes_tiles = dict()
		for (dim, size) in dsizes_scheme.items():
			for k in range(len(llsplit_scheme)):
				tilevar_name = "T" + dim + "_" + str(k)
				dsizes_tiles[tilevar_name] = size

		dsol = dsol | dsizes_tiles
		#print(dsol)

		val_cost_func_lvl = eval(str(cost_func_lvl), dsol)
		print(f"* Level {lvl} - cost_func = {int(val_cost_func_lvl)} - Scheme = {llsplit_scheme[i]}")
		i += 1
	# Levels above
	for k in range(i, len(llsplit_scheme)):
		inner_scheme = inner_scheme + llsplit_scheme[k]
		print(f"* Above (scheme part: {llsplit_scheme[k]})")


	# 2) Global cost
	dsol = dict()
	dsol = dsol | param_values

	# Note: inner_scheme now contains the full scheme
	dsizes_scheme = get_sizes_scheme(inner_scheme, ldims)
	dsizes_tiles = dict()
	for (dim, size) in dsizes_scheme.items():
		for lvl in range(len(llsplit_scheme)):
			tilevar_name = "T" + dim + "_" + str(lvl)
			dsizes_tiles[tilevar_name] = size

	dsol = dsol | dsizes_tiles

	val_glob_cost = eval(str(combined_cost_func), dsol)
	print(f"=> Global cost = {int(val_glob_cost)}")
	
	return



# =====================================
# =====================================
# =====================================


# Given a scheme and its corresponding Ioopt problem
#	gives back informations about the value of its cost function per levels
#
# vector_len = size (in float) of the vector on the machine (ex: AVX512 = 16 | AVX2 = 8)
def evaluate_cost_function(ioopt_filename : str, ttile_scheme: str, vector_len : int):
	scheme = parse_ttile_scheme(ttile_scheme, vector_len)
	#print(scheme)

	# Extracting cost functions from ioopt
	(prog, microkernel, lcachesizes, lpbs, param_values) = ioopt.gen_problem_from_yaml(
		ioopt_filename, is_filename=True, microk=None,
		cache=None, nlevels=None, b_integer_sol=False)

	# Simple curiosity
	#print(f"Debug - number of problems = {len(lpbs)}")

	# Split scheme in levels (according to its footprints and the cache sizes)
	llsplit_scheme = split_scheme_into_level(scheme, prog, lcachesizes)
	
	# DEBUG
	#print(llsplit_scheme)


	# Extract the cost functions
	# Issue: find the cost function that matches the permutation ? (which one? :/ )
	#
	# Sol 1: do a solve, then take the best permutation
	# Sol 2: try to match the scheme with a permutation
	#		(risk several sol matches / or none | difficult because of "1", permut not taken)
	#	Think about removing the microkernel
	# Sol 3: do for all cost function/problem sizes (only 5 of them...)
	# ===> Option 1 matches the best the experiment we wish to perform

	sol, best_perm = ioopt.solve(lpbs, param_values)
	best_pb = lpbs[ best_perm[1] ][1]
	dcost_func =  best_pb.expressions['cost']
	combined_cost_func = best_pb.f

	# dcost_func : dictionnary "L[i]" (1<=i ...) to the cost function at this level
	#print(dcost_func)

	# Print out info about the solution found by the solver
	print_cost_func_sol_info(dcost_func, param_values, sol[best_perm[1]])
	

	# Uses these cost-function on our scheme (that might differ from the solution)
	# Print-out the values of the different cost_func for our scheme
	print_cost_func_scheme_info(llsplit_scheme, prog.dims,
			combined_cost_func, dcost_func, param_values)

	return



# Main function
def main():
	# Testing
	ioopt_filename = "cnn_pinocchio.yml"
	#scheme_str = "[V F; U (3, H); U(7, Y); T (256, C);  Hoist_vars [C]; T(7, X); T(3, W); T(4, C); T(128, F)]"
	scheme_str = "[V F; U (4, F); U (4, Y); Tile_partial (32, C); Hoist_vars [C];\
				 Tile_partial (23, X); Tile_partial (48, Y); T(4, F);  Tile_gexact (96, C);\
				 Tile_exact (256, C); Tile_exact (68, X);  Tile_exact (68, Y)]"

	vector_len = 8  # float in AVX2
	evaluate_cost_function(ioopt_filename, scheme_str, vector_len)

	return

    
if __name__ == '__main__':
    main()

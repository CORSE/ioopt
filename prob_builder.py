# ==========================================================================
#    Copyright (C) INRIA
#    Contributors: Auguste Olivry, Guillaume Iooss
#    Date of creation: 2020 - 2021
#
#    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr
#
#    This software is a computer program whose purpose is to derive
#    automatically a lower-bound to the IO-complexity of a polyhedral
#    program.
#
#    This software is governed by the CeCILL  license under French law and
#    abiding by the rules of distribution of free software.  You can  use, 
#    modify and/ or redistribute the software under the terms of the CeCILL
#    license as circulated by CEA, CNRS and INRIA at the following URL
#    "http://www.cecill.info". 
#
#    As a counterpart to the access to the source code and  rights to copy,
#    modify and redistribute granted by the license, users are provided only
#    with a limited warranty  and the software's author,  the holder of the
#    economic rights,  and the successive licensors  have only  limited
#    liability. 
#
#    In this respect, the user's attention is drawn to the risks associated
#    with loading,  using,  modifying and/or developing or reproducing the
#    software by the user in light of its specific status of free software,
#    that may mean  that it is complicated to manipulate,  and  that  also
#    therefore means  that it is reserved for developers  and  experienced
#    professionals having in-depth computer knowledge. Users are therefore
#    encouraged to load and test the software's suitability as regards their
#    requirements in conditions enabling the security of their systems and/or 
#    data to be ensured and,  more generally, to use and operate it in the 
#    same conditions as regards security. 
#
#    The fact that you are presently reading this means that you have had
#    knowledge of the CeCILL license and that you accept its terms.
# ==========================================================================

import yaml
import sys
import isl
import os
import string
from typing import List, Dict, Set, Optional, Tuple
from sympy import symbols, sympify, ones, pprint, factor, Symbol, pretty, simplify
from sympy.parsing.sympy_parser import parse_expr
import itertools
from memoization import cached
from copy import deepcopy

from prob import MinProblem
import cfg

import symbolic_bounds

__debug=False


# ================================================
# Pretty printing functions
# ================================================


def bold(string):
    return '\033[1m'+string+'\033[0m'

TAB=5
def tab(numSpaces=TAB):
    return numSpaces * ' '

def reindent(s, numSpaces=TAB):
    if type(s) is Program: s=Program.__repr__(s)
    s = s.splitlines()
    s = [(numSpaces * ' ') + line for line in s]
    s = '\n'.join(s)
    return s

def sep_str(lst, sep):
    s = ''
    n = len(lst)
    for i in range(n):
        s += str(lst[i])
        if i < n - 1:
           s += sep
    return s

def pretty_permutation(perm, sol, microkernel=None):
    cdepth=len(perm)-1
    lperm=[]
    if microkernel is not None:
        lperm.append('CPU:{' + ','.join(microkernel.keys())+'}')
    for l in range(-1,len(perm)):
        tmp=perm[max(0,l)].copy()
        for d in reversed(perm[l]):
            size= sol[f'T{d}_{l+1}'] if l<cdepth else -1
            size_l= (1 if l==-1 else sol[f'T{d}_{l}'])
            if (l==-1 and microkernel is not None and d in microkernel) or (size==size_l):
                tmp.remove(d)
        lperm.append((f'L{l+2}:[' if l<cdepth else 'MEM:[') + ','.join(tmp) + ']')
    return '  '.join(lperm)
    
def pretty_schedule(perm, sol, param_values, microkernel=None, indent=''):
    tab=indent
    braces=''
    index_u={}
    size_u={}
    bound_u={}
    cdepth=len(perm)-1
    perm=list(perm)
    perm.insert(0,list())
    if microkernel is not None:
        perm[0][:]=list(microkernel.keys())
        for d in perm[1]:
            if d not in microkernel: perm[0].append(d)
    else: perm[0]=perm[1]
    for d in perm[0]:
        index_u[d]=0
        size_u[d]={param_values[f'N{d}']}
    for l in reversed(range(len(perm))):
        #print(f'{tab}//----L{l+1} loop(s)----')
        for d in reversed(perm[l]):
            size= sol[f'T{d}_{l}'] if l<=cdepth else param_values[f'N{d}']
            size_l= (1 if l==0 else sol[f'T{d}_{l-1}'])
            index=f'{d}{l}' if l>0 else f'{d}'
            if size_l!=size:
                if len(size_u[d])==1:
                    actsize=f'{size}'
                    print()
                else:
                    print(' {')
                    braces+='}'
                    print(f'{tab}S{index_u[d]}=min({size},{bound_u[d]}-{index_u[d]}) // in {size_u[d]}')
                    actsize=f'S{index_u[d]}'
                size=set()
                for su in size_u[d]:
                    if size_l<= su: size.add(size_l)
                    if su % size_l!=0: size.add(su % size_l)
                bound= f'{actsize}' if index_u[d]==0 else f'{index_u[d]}+{actsize}'
                if l==0 and microkernel is not None and d in microkernel:
                    print(f'{tab}//microkernel')
                    microkernel=None
                #print(tab, size_u[d], size_l, size)
                print(tab+bold('for')+f' ({index}={index_u[d]}; {index}<{bound}; {index}+={size_l})', end='')
                tab+='  '
                index_u[d]=index
                bound_u[d]=bound
                size_u[d]=size
    print()
    print(tab+'<bb>;')
    print(indent,braces)




# ================================================
# Core functions
# ================================================

# Data structure to store the input specification
class Program:
    dims : List[str]
    arrays : Dict[str, Tuple[List[str], int]]
    reuse : Optional[Dict[str, List[str]]] = None

    def __init__(self, dims, arrays, reuse  = None):
        self.dims = dims
        self.arrays = arrays
        self.reuse = reuse

    def __repr__(self) -> str:
        return 'dims: {}\narrays:\n{}\nreuse:\n{}\n'.format(
                sep_str(self.dims, ', '),
                sep_str(list(map(lambda a: '   - {}[{}] * {}'.format(a[0], sep_str(a[1][0], ', '), a[1][1]), self.arrays.items())), '\n'),
                sep_str(list(map(lambda a: '   - {}: {}'.format(a[0], sep_str(a[1], ', ')), self.reuse.items())) if self.reuse is not None else [], '\n')
                )

    
# Heuristic for permutation selection (based on the reuse)
def gen_permutations(prog: Program, microkernel = None) -> List[List[str]]:
    assert prog.reuse is not None

    def gen_perm(dct):
        if dct == {}:
            return [[]]
        perms = []

        taken = []
        for dim, arr in dct.items(): 
            if arr in taken:
                continue
            take = True
            for a in dct.values():
                if arr < a:
                    take = False
                    break
            if not take:
                continue
            new_dct = {}
            if len(arr) == 0:
                taken.append(arr)
            for d,a in dct.items():
                if d != dim:
                    new_dct[d] = a & arr
            p = gen_perm(new_dct)
            for q in p:
                perms.append([dim] + q)

        return perms

    rev_dict : Dict[str, Set[str]] = {}
    for d in prog.dims:
        rev_dict[d] = set()
    for array, dims_arr in prog.reuse.items():
        for d in dims_arr:
            rev_dict[d].add(array)
    

    perms = gen_perm(rev_dict)
    return perms


# From a relevant list of permutation at a given level, transform them into a multi-level combination of permutation
@cached
def gen_multi_level_permutation(prog: Program, n_levels : int, microkernel=None):
    # Get the relevant permutations
    if microkernel is not None:
        perms_l1 = gen_permutations(prog, microkernel)
    perms = gen_permutations(prog)
    
    if cfg.verbose:
        print(bold('Single-level permutations (inner to outer)'))
        for p in perms:
            print(tab(),f'{p}', flush=True)
        if microkernel is not None:
            print('\nMicrokernel permutations (inner to outer):\n')
            for p in perms_l1:
                print(tab(),f'{p}', flush=True)
        print('\n')

    multi_perms = list(itertools.product(perms, repeat=n_levels))
    if microkernel is not None:
        multi_perms = [(a, *b) for a in perms_l1 for b in list(itertools.product(perms, repeat=n_levels-1))]
    #print([list(enumerate(mp, start=1)) for mp in multi_perms])

    return multi_perms


# Build the MinProblem for a given cache level
@cached
def cost_fun(prog : Program, perm : List[str], level : int, max_level : int,
        b_integer_sol : bool, microkernel, b_no_data_vol : bool, lfp_penality : List[List[int]]) -> MinProblem:
    assert prog.reuse is not None

    ranges = {}
    for d in perm:
        ranges[d] = f'T{d}_{level-1}'

    dims_str = '[' + sep_str(prog.dims, ',') + ']'
    isl_arrays = dict([(arr, isl.union_map('{' + dims_str + '->' + arr + '[' + sep_str(l[0], ',') + '] : }')) for (arr, l) in prog.arrays.items()])

    all_vars = list(ranges.values())

    reused = dict([(a,False) for a in prog.arrays])

    # fp = footprint of an array (starting from the inner tile at 1)
    # TODO: what about the microkernels ? Does that change this first footprint?
    fp = dict([(a,1) for a in prog.arrays])
    cost = dict()
    nops = sympify(sep_str(list(ranges.values()), '*'), locals={'Ne':Symbol('Ne')})

    params = [Symbol(f'C_{level}')] + [Symbol(f'N{d}') for d in perm]

    def update_cost():
        # Building the isl set corresponding to the iteration domain of the current tile
        isl_ranges_str = sep_str([f'0 <= {dim} < {ranges[dim]}' for dim in perm], ' and ')
        isl_idx_set = isl.union_set('[' + sep_str(all_vars + params, ',') + '] -> { ['
                + sep_str(prog.dims, ',') + '] : ' + isl_ranges_str + '}')

        for arr in prog.arrays:
            # If no reuse, we need to recompute the footprint of the current array
            if not reused[arr]:
                # Compute the new footprint of the array
                arr_s = isl_idx_set.apply(isl_arrays[arr])

                # card_arr_str : cardinality of the footprint of the array, given tile sizes
                #   (done by extracting the isl expression, ignoring the conditions/branching)
                card_arr_str = str(arr_s.card())
                i_left = card_arr_str.find('{')+1
                i_right = card_arr_str.find(':')-1
                card_arr_str = card_arr_str[i_left:i_right]
                
                # Cleaning up things
                card_arr = sympify(card_arr_str, locals={'Ne':Symbol('Ne')})
                card_arr = factor(card_arr)

                # Saving the footprint of the array
                fp[arr] = card_arr

    rng_diff = []
    update_cost()

    # First constraint: Constraint on the footprint
    if lfp_penality==[]:
        fp_constr = sum(fp.values())
    else:
        # Cache associativity: weight sum with the penalities of lfp_penality
        lfpexpr = list(fp.values())
        assert(len(lfpexpr) == len(lfp_penality))
        lweighted_fpexpr = []
        for k in range(len(lfpexpr)):
            lweighted_fpexpr.append( lfp_penality[k][0] * lfpexpr[k] / lfp_penality[k][1] )

        fp_constr = sum(lweighted_fpexpr)

    g = [fp_constr]

    # Rest of the constraints: being bigger than the inner sub-tile (microkernel if lvl=0)

    # Naming convention for the tile variable names
    def tile_var_name(d,level):
        return f'T{d}_{level}'

    for d in perm: 
        rng0 = tile_var_name(d, level-1)
        rng1 = f'N{d}'
        if level < max_level:
            rng1 = tile_var_name(d,level)
            all_vars.append(rng1)
        ranges[d] = rng1 
        update_cost()
        nops = nops.subs(rng0, Symbol(rng1))
        for arr in prog.arrays:
            if not reused[arr] and d not in prog.reuse[arr]:
                reused[arr] = True
                cost[arr] = fp[arr]/nops
        if level < max_level:
            rng_diff.append(Symbol(rng1) - Symbol(rng0))

    g = g + rng_diff

    X = all_vars
    X_lim = []

    for i in range(len(perm)):
        d = perm[i]
        p = params[i+1]
        if microkernel==None:
            min_val_tile_size = 1
        elif d in microkernel.keys():
            min_val_tile_size = microkernel[d]
        else:
            min_val_tile_size = 1

        X_lim.append((sympify(min_val_tile_size), p))
    if level < max_level:
        X_lim = X_lim * 2

    # Default values needs to be specified by another function
    X_default = []
    for i in range(len(perm)):
        X_default.append(None)
    if level < max_level:
        X_default = X_default * 2


    #print(perm, cost)

    f = sum([prog.arrays[arr][1] * cost[arr] for arr in prog.arrays])
    # If b_no_data_vol, we do not multiply by the product of the sizes
    #   => This gives wrong data volume informations, but
    #   Allows us to have a smaller objective function, which makes
    #   some solver (couenne) works better with larger problem sizes.
    if (not b_no_data_vol):
        for d in perm:
            f *= Symbol(f'N{d}')
    g_lim = [(0, params[0])]
    if level < max_level:
        g_lim = [(0, p) for p in params]

    expressions = {
            'cost' : { f'L{level}': f},
            'footprint' : { f'L{level}': g[0]}
        }


    # If integral solving, add constraints to force the variables from X to be multiples
    #       of the microkernel sizes
    X_mult_mickern = []
    g_div = []
    if ((b_integer_sol) and microkernel!=None):
        for d, micker_size_d in microkernel.items():
            # Sanity check: microkernel divides the problem size
            # Does not work: params[ivname+1] is still symbolic
            #assert(d in perm)
            #ivname = perm.index(d)
            #assert(params[ivname+1] % micker_size_d == 0)

            # Create the new variable for the divisibility constraint
            var_div_name = f'mult_micker_{d}_{level-1}'
            X_mult_mickern.append(sympify(var_div_name))

            # Create the new divisibility constraint: "var_div_name * micker_size_d - T{d}_{level-1} == 0"
            div_constr = f'{var_div_name} * {micker_size_d} - {tile_var_name(d,level-1)}'
            g_div.append(sympify(div_constr))

    if __debug:
        print(bold('Pb:'))
        print(tab(),'params: ',params)
        print(tab(),'X: ', X)
        print(tab(),'f: ',f)
        print(tab(),'g: ',g)
        print(tab(),'Xlim: ',X_lim)
        print(tab(),'X_default: ',X_default)
        print(tab(),'g_lim: ',g_lim)
        print(tab(),'expressions: ',expressions)
        print(tab(),'b_integer_sol: ',b_integer_sol)
        print(tab(),'X_mult_mickern: ',X_mult_mickern)
        print(tab(),'g_div: ',g_div)

    # Finally calling the constructor to store all of these infos (cf prob.py)
    return MinProblem(params, X, f, g, X_lim, X_default, g_lim, expressions, b_integer_sol,
            X_mult_mickern, g_div, microkernel)
            

# Combine problems (i.e., variables and constraints) from different tile levels
@cached
def combine_pbs(pb_list: List[MinProblem], bw: List[float], b_integer_sol : bool) -> MinProblem:
    P_comb : List[Symbol] = list(set().union(*[set(p.P) for p in pb_list]))

    # Note: redundancies exists between the X/X_lim/X_default of each problem
    # By construction, we assume that the values are the same whenever there is a redundancy
    X_comb = []
    X_lim_comb = []
    X_default_comb = []
    for p in pb_list:
        #print(p.X)
        #print(p.X_lim)
        #print(p.X_default)

        for i in range(len(p.X)):
            # Element already un X_comb => skip
            if (p.X[i] in X_comb):
                continue
            X_comb.append(p.X[i])
            X_lim_comb.append(p.X_lim[i])
            X_default_comb.append(p.X_default[i])

    # Old Code
    #all_X = itertools.chain(*[p.X for p in pb_list])
    #all_X_lim = itertools.chain(*[p.X_lim for p in pb_list])
    #X_dict = dict([xxl for xxl in zip(all_X, all_X_lim)]) # OK because limits are always the same
    #X_comb, X_lim_comb = list(X_dict.keys()), list(X_dict.values())
    
    f_sum = sum([b*p.f for (b,p) in zip(bw, pb_list)])

    g_comb = list(itertools.chain(*[p.g for p in pb_list]))
    g_lim_comb = list(itertools.chain(*[p.g_lim for p in pb_list]))

    X_mult_mickern_comb = list(itertools.chain(*[p.X_mult_mickern for p in pb_list]))
    g_div_comb = list(itertools.chain(*[p.g_div for p in pb_list]))

    #print("DEBUG", g_comb, g_lim_comb)
    expr_comb = {'cost' : {}, 'footprint' : {}}
    for p in pb_list:
        expr_comb['cost'].update(p.expressions['cost'])
        expr_comb['footprint'].update(p.expressions['footprint'])

    # All microkernel should be the same
    microkernel = pb_list[0].microkernel

    return MinProblem(P_comb, X_comb, f_sum, g_comb, X_lim_comb, X_default_comb, g_lim_comb, expr_comb,
        b_integer_sol, X_mult_mickern_comb, g_div_comb, microkernel)


# Problem generation from an input specification (main function)
@cached
def gen_problems(prog: Program, n_levels : int, bw=None, microkernel=None, b_integer_sol=False,
        b_no_data_vol=False, lfp_penality=[]):
    # Get the multi-level permutation candidates
    multi_perms = gen_multi_level_permutation(prog, n_levels, microkernel)

    if bw == None:
        bw = [1]*n_levels
    
    """
    # Old portion of code, similar to the next block, but harder to adapt/modify
    pbs = [combine_pbs(
        list(
            map(lambda a : cost_fun(prog, a[1], a[0], n_levels, b_integer_sol,
                    microkernel, b_no_data_vol, lfp_penality)
                , enumerate(mp, start=1)
            )
        ),
        bw, b_integer_sol) for mp in multi_perms]
    """
    # Generate optimisation problem for each levels (cost_fun), then combine them (combine_pbs)
    pbs = []
    for mp in multi_perms:
        lpbs_multi_perm = []
        for lvl in range(len(mp)):
            perm = mp[lvl]

            # Penality only for the last level (else, issue with the per_lvl solving)
            if lvl<len(mp)-1:
                lfp_pen = []
            else:
                lfp_pen = lfp_penality

            # Build the problem for each level
            pb_lvl = cost_fun(prog, perm, lvl+1, n_levels, b_integer_sol,
                microkernel, b_no_data_vol, lfp_pen)

            lpbs_multi_perm.append(pb_lvl)

        # Combine the problems across all levels
        comb_pbs = combine_pbs(lpbs_multi_perm, bw, b_integer_sol)
        pbs.append(comb_pbs)



    # Optimisation to remove redundancy
    unique_exprs = set()
    unique_pbs = []
    for i in range(len(pbs)):
        if not pbs[i].f in unique_exprs:
            unique_pbs.append((multi_perms[i], pbs[i]))
            unique_exprs.add(pbs[i].f)

    return unique_pbs


# Add default values to a problem (to speed-up solving)
def add_default_value_to_pbs(pbs, Xval_def):
    #print(Xval_def)
    for pb in pbs:
        # Xval_def : [variable name] -> [default value]
        # Need to be converted into a [variable index] -> [default value]
        nXdef = []

        #print(pb)
        
        for _ in pb[1].X:
            nXdef.append(None)  # By default, no default value

        # Add the default values
        for (var_name, def_val) in Xval_def.items():
            i = pb[1].X.index(var_name)    # No ValueError should happen here
            nXdef[i] = def_val

        pb[1].X_default = nXdef

    return pbs


# Call the solver on the list of MinProblem (one per permutation) pbs
#       and select the best resulting one
def solve(pbs, param_values):
    best_perm = None
    sol = []
    for i in range(len(pbs)):
        if cfg.verbose:
            print(f'\n\n SOLVING FOR PERMUTATION {i+1} / {len(pbs)}....\n\n', flush=True)
        s, v = pbs[i][1].solve(param_values)

        # s (dict): X (variable to be solved) --> value of X  (converted to integer later)
        # v: value of the objective function
        
        #round tile sizes to get integer values and reevaluate
        for x in s:
            s[x] = int(s[x])
        v = pbs[i][1].f.subs({**param_values, **s}).evalf()

        sol.append((s, v))
        if best_perm == None or v < best_perm[0]:
            best_perm = (v, i)
        # best_perm : [ value_of_obj_func, ind_best_problem ]
    return sol, best_perm


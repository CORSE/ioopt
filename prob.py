# ==========================================================================
#    Copyright (C) INRIA
#    Contributors: Auguste Olivry
#    Date of creation: 2020 - 2021
#
#    Emails: auguste.olivry@inria.fr
#
#    This software is a computer program whose purpose is to derive
#    automatically a lower-bound to the IO-complexity of a polyhedral
#    program.
#
#    This software is governed by the CeCILL  license under French law and
#    abiding by the rules of distribution of free software.  You can  use, 
#    modify and/ or redistribute the software under the terms of the CeCILL
#    license as circulated by CEA, CNRS and INRIA at the following URL
#    "http://www.cecill.info". 
#
#    As a counterpart to the access to the source code and  rights to copy,
#    modify and redistribute granted by the license, users are provided only
#    with a limited warranty  and the software's author,  the holder of the
#    economic rights,  and the successive licensors  have only  limited
#    liability. 
#
#    In this respect, the user's attention is drawn to the risks associated
#    with loading,  using,  modifying and/or developing or reproducing the
#    software by the user in light of its specific status of free software,
#    that may mean  that it is complicated to manipulate,  and  that  also
#    therefore means  that it is reserved for developers  and  experienced
#    professionals having in-depth computer knowledge. Users are therefore
#    encouraged to load and test the software's suitability as regards their
#    requirements in conditions enabling the security of their systems and/or 
#    data to be ensured and,  more generally, to use and operate it in the 
#    same conditions as regards security. 
#
#    The fact that you are presently reading this means that you have had
#    knowledge of the CeCILL license and that you accept its terms.
# ==========================================================================

from typing import List, Dict, Set, Optional, Tuple
import numpy
import sympy
import math
from sympy import symbols, sympify, ones, pprint, factor, Symbol, simplify
from sympy.matrices import Matrix, hessian
from amplpy import AMPL, Environment

import cfg

def make_sparsity(M, sym=False):
    s = ([],[])
    for i in range(M.rows):
        for j in range(M.cols):
            if (not (sym and i < j)) and M[i,j] != 0:
                s[0].append(i)
                s[1].append(j)
    return (numpy.array(s[0]), numpy.array(s[1]))

class MinProblem:

    P: List[sympy.Symbol] # parameters
    X: List[sympy.Symbol] # variables
    f: sympy.Basic # objective function
    g: List[sympy.Basic] # constraint functions
    X_lim: List[Tuple[sympy.Basic,sympy.Basic]] #X_L and X_U
    g_lim: List[Tuple[sympy.Basic,sympy.Basic]] #g_L and g_U
    expressions: Dict[str, Dict[str, sympy.Basic]]

    X_mult_mickern: List[sympy.Symbol]
    g_div: List[sympy.Basic]

    def __init__(self, P, X, f, g, X_lim, X_default, g_lim, e,
                b_integer_sol=False, X_mult_mickern=[], g_div=[], microkernel=None):
        self.P = P                    # Parameters (unused)

        self.X = X                    # List of variables (tile sizes across all dimensions)
        self.f = f                    # Objective function
        self.g = g                    # List of constraints expression

        self.X_lim = X_lim            # List of (min_val, max_val), same index than self.X
        self.X_default = X_default    # List of default value, value is None if key has no default value
        self.g_lim = g_lim            # List of (lower_val, upper_val) for constraints

        self.expressions = e          # Unused / contains 2 fields: 'cost' and 'footprints',
                                      #   both maps associated string f'L{i}' (for level i) to the expression

        self.b_integer_sol = b_integer_sol   # Should we use an integer solver ?
        self.X_mult_mickern = X_mult_mickern # List of extra vars to ensure div. with the microkernel
        self.g_div = g_div                   # List of equality constraints (=0) enforcing divisibility
        self.microkernel = microkernel       # dict: d (dimension) |-> micker_size_d (integer)

    def __repr__(self) -> str:
        return f'X: {self.X}\nf: {self.f}\ng: {self.g}\nX_lim: {self.X_lim}\ng_lim:{self.g_lim}\n'


    # Correct the lower/upper bound (X_lim) to be multiple of the microkernel
    # Should be triggered only in the case of an integral solving, with a microkernel
    def preprocessing_bound_mickern(self, param_val):
        assert(self.b_integer_sol)
        assert(self.microkernel!=None)

        for i in range(len(self.X)):
            varname = self.X[i]

            # To deal with the "mult_micker_[dim]_[lvl]" variables
            if (not varname.startswith("T")):
                continue

            # Using the naming convention here: "TH_0"
            dimname = varname.split("_")[0][1:]
            dimname = dimname.lower()

            #print(varname)
            #print(dimname)

            if dimname in self.microkernel:
                mickersize = self.microkernel[dimname]
                (lbound, ubound) = self.X_lim[i]

                lbound = lbound.subs(param_val)
                ubound = ubound.subs(param_val)

                # lbound => increase to the nearest multiple of micker_size_d
                if (lbound % mickersize != 0):
                    nlboundval = mickersize * math.ceil(int(lbound) / mickersize)
                    nlbound = sympify(nlboundval)
                else:
                    nlbound = lbound

                # ubound => decrease to the nearest multiple of micker_size_d
                if (ubound % mickersize != 0):
                    nuboundval = mickersize * math.floor(int(ubound) / mickersize)
                    nubound = sympify(nuboundval)
                else:
                    nubound = ubound

                self.X_lim[i] = (nlbound, nubound)

        return


    # Build a string corresponding to the AMPL problem (sent to the solver)
    def build_str_ampl_problem(self, param_val: Dict[str,int]):
        nvar = len(self.X)
        ncon = len(self.g)

        mod_str = ''
        #variable ranges
        for i in range(nvar):
            # Example: "var TH_2 >= 1, <= 3 integer default 0;"
            # The "integer" and "default 0" are both optional
            if self.b_integer_sol:
                integer_part = ' integer'
            else:
                integer_part = ''

            if (self.X_default[i] != None):
                default_val_part = f' default {self.X_default[i]}'
            else:
                default_val_part = ''

            if self.X[i] in param_val:
                mod_str += f'var {self.X[i]} = {param_val[self.X[i]]}'
            else:
                mod_str += f'var {self.X[i]} >= {self.X_lim[i][0]}, <= {self.X_lim[i][1].subs(param_val)}'

            mod_str += integer_part + default_val_part + ';\n'
        mod_str += '\n'

        # Extra integer variable
        if self.b_integer_sol:
            for var_i in self.X_mult_mickern:
                mod_str += f'var {var_i} >= 1 integer;\n'
        mod_str += '\n'

        #objective function
        obj_fun_subs = self.f.subs(param_val)
        obj_fun_subs = obj_fun_subs.simplify().expand()
        mod_str += f'minimize obj:\n\t{obj_fun_subs};\n'

        #constraints
        for i in range(ncon):
            mod_str += f's.t. gu{i}:\n\t{self.g[i]}  >= {self.g_lim[i][0]};\n'
            mod_str += f's.t. gl{i}:\n\t{self.g[i]}  <= {self.g_lim[i][1].subs(param_val)};\n'

        # Extra constraints for divisibility
        if self.b_integer_sol:
            for i in range(len(self.g_div)):
                mod_str += f's.t. gdiv{i}:\n\t{self.g_div[i]} = 0;\n'

        return mod_str


    # Main function - solve a problem using AMPL
    def solve(self, param_val: Dict[str,int]):

        # Preprocessing on the bound, to ensure they divide the microkernel sizes
        if (self.b_integer_sol and (self.microkernel!=None)):
            self.preprocessing_bound_mickern(param_val)

        # Get the AMPL problem statement
        mod_str = self.build_str_ampl_problem(param_val)

        if cfg.verbose:
            print('\nSolving:\n', mod_str)

        # DEBUG
        #print(mod_str)

        # Solving time !
        ampl = AMPL()#env)
        if self.b_integer_sol:  # If integer solving
            #ampl.setOption('couenne_options', 'print_level=0')
            #ampl.setOption('couenne_options', 'display_stats=no')
            ampl.setOption('solver', 'couenne')
        else:  # Default option
            ampl.setOption('ipopt_options', 'print_level=0')
            ampl.setOption('solver', 'ipopt')
        ampl.eval(mod_str)
        ampl.solve()

        # Extract the result found by the solver
        obj = ampl.getObjective('obj').value()
        X_val = dict([(x, round(ampl.getVariable(str(x)).value(), 2)) for x in self.X])
        ret = X_val, round(obj, 8)

        if cfg.verbose:
            print('\nSolution:\n', ret)

        return ret

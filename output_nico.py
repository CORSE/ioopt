# ==========================================================================
#    Copyright (C) INRIA
#    Contributors: Auguste Olivry
#    Date of creation: 2020 - 2021
#
#    Emails: auguste.olivry@inria.fr
#
#    This software is a computer program whose purpose is to derive
#    automatically a lower-bound to the IO-complexity of a polyhedral
#    program.
#
#    This software is governed by the CeCILL  license under French law and
#    abiding by the rules of distribution of free software.  You can  use, 
#    modify and/ or redistribute the software under the terms of the CeCILL
#    license as circulated by CEA, CNRS and INRIA at the following URL
#    "http://www.cecill.info". 
#
#    As a counterpart to the access to the source code and  rights to copy,
#    modify and redistribute granted by the license, users are provided only
#    with a limited warranty  and the software's author,  the holder of the
#    economic rights,  and the successive licensors  have only  limited
#    liability. 
#
#    In this respect, the user's attention is drawn to the risks associated
#    with loading,  using,  modifying and/or developing or reproducing the
#    software by the user in light of its specific status of free software,
#    that may mean  that it is complicated to manipulate,  and  that  also
#    therefore means  that it is reserved for developers  and  experienced
#    professionals having in-depth computer knowledge. Users are therefore
#    encouraged to load and test the software's suitability as regards their
#    requirements in conditions enabling the security of their systems and/or 
#    data to be ensured and,  more generally, to use and operate it in the 
#    same conditions as regards security. 
#
#    The fact that you are presently reading this means that you have had
#    knowledge of the CeCILL license and that you accept its terms.
# ==========================================================================

import sys
from typing import List, Dict, Set, Optional, Tuple
import yaml

# Aux function: check if there is a key in dict1, else check in dict2
def get_val_2maps(dict1, dict2, key):
    if (key in dict1):
        return dict1[key]
    else:
        return dict2[key]


# Return a "Ttile"-like strategy scheme, with one element per line
# Example of output (on cnn.yml with the microkernel line uncommented):
#       " V f_dim; U (2, f_dim); U (12, y_dim); T (20, x_dim);
#           (* End L1 *) R w_dim; R h_dim; R c_dim; R x_dim; R y_dim; (* End L2 *) "
def output_nico(perms: Tuple[List[str]], val: Dict[str, float], microkernel: Dict[str, int]):
    dim_sz = dict(zip(perms[0], [1]*len(perms[0])))

    muk = list(microkernel.items())
    d0 = muk[0][0]

    out = f'V {d0}_dim;\n' #vectorize innermost dimension (*16)
    unroll = muk[0][1]//16
    if unroll > 1:
        out += f'U ({unroll}, {d0}_dim);\n'
    dim_sz[d0] = muk[0][1]
    for d, u in muk[1:]:
        out += f'U ({u}, {d}_dim);\n'
        dim_sz[d] = u

    p0 = []
    for d in perms[0]:
        if d not in microkernel:
            p0.append(d)
    for lvl, p_i in enumerate((p0,) + perms):
        for d in p_i:
            if lvl == len(perms):
                v = int(val[f'N{d}'])
            else:
                v =  int(val[f'T{d}_{lvl}'])
            if v == dim_sz[d]:
                continue
            if v == val[f'N{d}']:
                out += f'R {d}_dim;\n'
            else:
                out += f'T ({v}, {d}_dim);\n'
            dim_sz[d] = v
        out += f'(* End L{lvl+1} *)\n'

    return out

# Return only the permutation found
# Example of output (on cnn.yml with the microkernel line uncommented):
#        [['X'], ['W', 'H', 'C', 'X', 'Y']]
def output_permonly(perms: Tuple[List[str]], val: Dict[str, float], microkernel):
    #p0 = list(microkernel.keys()) # Uncomment to include microkernel in permutation
    p0 = []
    for d in perms[0]:
        if d not in microkernel:
            p0.append(d)

    pout = []
    dim_sz = dict(zip(perms[0], [1]*len(perms[0])))
    for d in microkernel:
        dim_sz[d] = microkernel[d]
    for lvl, p_i in enumerate((p0, ) + perms):
        pout.append([])
        for d in p_i:
            if lvl == len(perms):
                v = int(val[f'N{d}'])
            else:
                v =  int(val[f'T{d}_{lvl}'])
            if v != dim_sz[d]:
                pout[lvl].append(d.upper());
            dim_sz[d] = v
    return pout


# Return the permutation and the sizes found for each dimensions
# Example of output:  " [[('X', 20)], [('W', 3), ('H', 3), ('C', 128), ('X', 96), ('Y', 96)]]"
# Note: these are the sizes of the tiles, not the ratio
def output_permsizes(perms: Tuple[List[str]], val: Dict[str, float], sol : Dict[str, float], microkernel):
    #p0 = list(microkernel.keys()) # Uncomment to include microkernel in permutation
    # p0: all the permutations not in the microkernel
    p0 = []
    for d in perms[0]:
        if d not in microkernel:
            p0.append(d)

    ps_out = []

    # dim_sz = current tile size along every dimension
    #  (by default 1^n, except on the microkernel)
    dim_sz = dict(zip(perms[0], [1]*len(perms[0])))
    for d in microkernel:
        dim_sz[d] = microkernel[d]

    # For each memory level...
    for (lvl, p_i) in enumerate((p0, ) + perms):
        ps_out.append([])
        for d in p_i:
            if lvl == len(perms):
                # Problem size for the last level
                value = get_val_2maps(sol, val, f'N{d}')
                v = int(value) 
            else:
                # Tile size
                value =  get_val_2maps(sol, val, f'T{d}_{lvl}') 
                v = int(value) 

            # If the new size is greater than the tile size of the previous level,
            # Update it
            if v != dim_sz[d]:
                # TODO: change that !!!
                ps_out[lvl].append((d.upper(), v));
            dim_sz[d] = v
    return ps_out


# Remove the repetitions of a scheme in a list of scheme
def unique_schemes(lscheme):

    # Do a str convertion on scheme and use these (with a dict) to get unicity
    lscheme_unique = []
    salready_seen = set()
    for scheme in lscheme:
        str_scheme = str(scheme)

        if str_scheme not in salready_seen:
            salready_seen.add(str_scheme)
            lscheme_unique.append(scheme)

    return lscheme_unique


if __name__ == '__main__':
    with open(sys.argv[1]) as fprog :
        y = yaml.load(fprog.read(), Loader=yaml.SafeLoader)
        val = {}
        for i,d in enumerate(y['dims']):
            val[f'N{d}'] = y['values']['dims'][i]
        val.update( y['solution']['values'])
        out = output_nico(y['solution']['perms'], val, y['values']['microkernel'])
        print(out)

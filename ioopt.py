#!/usr/bin/env python3

# ==========================================================================
#    Copyright (C) INRIA
#    Contributors: Auguste Olivry, Guillaume Iooss
#    Date of creation: 2020 - 2021
#
#    Emails: auguste.olivry@inria.fr, guillaume.iooss@inria.fr
#
#    This software is a computer program whose purpose is to derive
#    automatically a lower-bound to the IO-complexity of a polyhedral
#    program.
#
#    This software is governed by the CeCILL  license under French law and
#    abiding by the rules of distribution of free software.  You can  use, 
#    modify and/ or redistribute the software under the terms of the CeCILL
#    license as circulated by CEA, CNRS and INRIA at the following URL
#    "http://www.cecill.info". 
#
#    As a counterpart to the access to the source code and  rights to copy,
#    modify and redistribute granted by the license, users are provided only
#    with a limited warranty  and the software's author,  the holder of the
#    economic rights,  and the successive licensors  have only  limited
#    liability. 
#
#    In this respect, the user's attention is drawn to the risks associated
#    with loading,  using,  modifying and/or developing or reproducing the
#    software by the user in light of its specific status of free software,
#    that may mean  that it is complicated to manipulate,  and  that  also
#    therefore means  that it is reserved for developers  and  experienced
#    professionals having in-depth computer knowledge. Users are therefore
#    encouraged to load and test the software's suitability as regards their
#    requirements in conditions enabling the security of their systems and/or 
#    data to be ensured and,  more generally, to use and operate it in the 
#    same conditions as regards security. 
#
#    The fact that you are presently reading this means that you have had
#    knowledge of the CeCILL license and that you accept its terms.
# ==========================================================================

import argparse
import yaml
import sys
import isl
import os
import string
import math
from typing import List, Dict, Set, Optional, Tuple
from sympy import symbols, sympify, ones, pprint, factor, Symbol, pretty, simplify
from sympy.parsing.sympy_parser import parse_expr
import itertools
from memoization import cached
from copy import deepcopy

from prob import MinProblem
from prob_builder import bold, TAB, tab, reindent, sep_str, pretty_permutation, pretty_schedule
from prob_builder import Program, gen_multi_level_permutation, gen_problems, add_default_value_to_pbs, solve
import cfg

from output_nico import output_nico, output_permonly, output_permsizes, unique_schemes

import symbolic_bounds

import cacheset_estimation

__debug=False


# Naming convention of parameters in param_values
def param_naming_conv(dimname):
    return f'N{dimname}'


# Extracting the relevant informations from the input specification
# Note: is_filename : "filename" is the name of a yaml file, and not directly its content (as dict)
def gen_problem_from_yaml(filename: str, is_filename=True, microk=None,
        cache=None, nlevels=None, b_integer_sol=False, b_no_data_vol=False):
    param_values = dict()
    microkernel = microk 
    bw = None
    
    # Read the input specification (as a string or as a file)
    if is_filename:
        fprog = open(filename,'r')
        y = yaml.load(fprog.read(), Loader=yaml.SafeLoader)
    else:
        y = filename

    p = Program(y['dims'], y['arrays'], y['reuse'])

    # Cache values + nlevels
    if cache is None:
        v = y['values']
        if nlevels is None: nlevels = v['n_levels']
    else:
        if nlevels is None: nlevels=len(cache)
        v['cache']=cache
    assert(nlevels<=len(v['cache']))

    # Bandwidths + param values
    if 'bw' in v:
        bw = []
    for l in range(nlevels):
        param_values[f'C_{l+1}'] = v['cache'][l]
        if 'bw' in v:
            bw.append(v['bw'][l])
    for i,d in enumerate(p.dims):
        param_values[param_naming_conv(d)] = v['dims'][i]
    
    # Microkernel values
    if 'microkernel' in v and microkernel is None:
        microkernel = v['microkernel']

    # Bug here => Microkernel should not fix the sizes of the first level of tiling
    #if microkernel is not None:
    #    for d, val in microkernel.items():
    #        param_values[f'T{d}_0'] = val

    # Cache infos
    if 'cache_line_size' in v:
        cache_line_size = v['cache_line_size']
    else:
        cache_line_size = 16  # Default value: 512 bits = 16 floats

    if 'cache_assoc' in v:
        cache_assoc = v['cache_assoc']
        assert(len(cache_assoc) >= nlevels)

        # Compute the number of cache sets
        lcache_set = []
        for l in range(nlevels):
            assert(v['cache'][l] % (cache_line_size * cache_assoc[l]) == 0)
            cache_set = int(v['cache'][l] / (cache_line_size * cache_assoc[l]))
            lcache_set.append(cache_set)
    else:
        lcache_set = []  # Mark a fully associative cache


    if cfg.verbose:
        print(bold('Input program'))
        print(reindent(p)+'\n')
        print(bold('Param values'))
        print(tab(),param_values,'\n')
        print(bold('Cache set info'))
        if lcache_set == []:
            print(tab(),"Full associativity",'\n')
        else:
            print(tab(),lcache_set,'\n')

    # We want to have precise info about data movement (simplification for integral solving)
    b_no_data_vol = False

    return (p, microkernel, v['cache'][0:nlevels],
        gen_problems(p, nlevels, bw, microkernel, b_integer_sol, b_no_data_vol),
        param_values,
        lcache_set, cache_line_size)



# [Experiment] Activate to use only a portion of the ideal size of the cache size (cf next function)
adaptation_expe = False

# Experiment: use only fraction of the full cache (because not ideal)
def adapting_cache_sizes(lcachessizes):
    nlcachessizes = []

    # L1d : put at 90%
    if (len(lcachessizes)>=1):
        nlcachessizes.append(int(0.9 * lcachessizes[0]))
    # L2: includes the L1i. Assuming a ratio of 8 between the L1d and the L2, put at 7/8
    if (len(lcachessizes)>=2):
        nlcachessizes.append(int( 7 * lcachessizes[1] / 8))

    # L3: in sequential, no modif done (yet?)
    nlcachessizes = nlcachessizes + lcachessizes[2:]

    return nlcachessizes



# ================================================
# Entry functions
# ================================================


# [Unused] Main Python function for external Python call
def get_perm_from_microkernel(filename: str, microkernel):
    _, microkernel, _, pbs, param_values, _, _ = gen_problem_from_yaml(filename, microk=microkernel)
    sol, best_perm = solve(pbs, param_values)
    param_values.update(sol[best_perm[1]][0])
    return output_permonly(pbs[best_perm[1]][0], param_values, microkernel)


# [Experimental - not conclusive] Use the rational solution to speed up the integral solving
def gen_and_solve_rat_to_int(filename: str, is_filename=True, cache=None, nlevels=None,
        microk=None, b_integer_sol=False, b_solve=False, b_verbose=False):
    assert(b_integer_sol==True)
    assert(b_solve==True)

    # Build the rational problem and solve it
    _, _, _, pbs, param_values, _, _ = gen_problem_from_yaml(filename, is_filename=is_filename,
        cache=cache, nlevels=nlevels, microk=microk, b_integer_sol=False)
    sol_rat, _ = solve(pbs, param_values)

    # Adapt the solution to make it possible
    Xval_rat = sol_rat[0][0]

    #print(Xval_rat)

    # If a microkernel is specified, we need to ensure divisibility
    if microk != None:
        for (varname, val) in Xval_rat.items():
            # Check the value of the microkernel on that dimension
            # Assume that varname follows the naming convention "T[dim]_[level]"
            assert(varname[0]=="T")
            tdim = varname.split("_")[0]
            dim = tdim[1:]

            # If no constraints from microkernel on that dim: skip
            if dim not in microk.keys():
                continue

            size_microk = microk[dim]
            # If divisibility does not match, round down the value
            if (val % size_microk != 0):
                nval = int( math.floor(val / size_microk) * size_microk)
                # If nval==0 (too low), make it equal to size_microk
                if (nval<size_microk):
                    nval = size_microk

                Xval_rat[varname] = nval



    # Build the integral problem and solve it
    _, microkernel, cache, pbs, param_values, _, _ = gen_problem_from_yaml(filename, is_filename=is_filename,
        cache=cache, nlevels=nlevels, microk=microk, b_integer_sol=True)
    # Uses Xval_rat as the initial solution
    pbs = add_default_value_to_pbs(pbs, Xval_rat)
    sol, best_perm = solve(pbs, param_values)

    return microkernel, cache, pbs, param_values, sol, best_perm


# Auxilliary function - Adapt the problem to facilitate integral solving
# Preprocess pbs_lvl_k (add previous solution + problem param value propagation)
def preprocess_problem_int_per_lvl(pbs_lvl_k, dprevsol, param_values):
    # Add the previous solution as equality in the problems
    # For each problems...
    for tuple_pb in pbs_lvl_k:
        #perm = pb[0]
        pb = tuple_pb[1]

        # Note: namevar is a "sympy.Symbol"
        for (namevar, val) in dprevsol.items():
            if (namevar in pb.X):
                i = pb.X.index(namevar)

                # Check that the value fit in the pbl constraints
                # Note: cannot do that (pb.X_lim[i][1] could be a parameter)
                #assert(int(pb.X_lim[i][0]) <= int(val))
                #assert(int(val) <= int(pb.X_lim[i][1]))

                # Impose equality
                pb.X_lim[i] = (sympify(val), sympify(val))
            elif (namevar in pb.X_mult_mickern):
                # We do nothing for the "mult_micker_[d]_[lvl]"
                continue
            else:
                continue

        # For each variable whose lower and upper bound are the same,
        #   do a substitution in the cost function
        # (else, potential issue with Couenne,
        #   cf https://github.com/coin-or/Couenne/issues/69 )

        # We substitute first the problem sizes (to enable more simplifications)
        #for (nameparam, val) in param_values.items():
        #    pb.f = pb.f.subs(nameparam, val)
        pb.f = pb.f.subs(param_values)

        for i in range(len(pb.X)):
            namevar = pb.X[i]
            lubounds = pb.X_lim[i]

            if (lubounds[0] == lubounds[1]):
                val = lubounds[0]
                pb.f = pb.f.subs(namevar, val)
        pb.f = simplify(pb.f)
    return pbs_lvl_k


# Auxilliary function - In case of rational solution, adapt it to integral
def adapt_rat_to_int_per_lvl(best_sol, microkernel):
    # If we are in rational (experiment), then round to the nearest multiple of the microkernel
    for (varname, val) in best_sol.items():
        # Check the value of the microkernel on that dimension
        # Assume that varname follows the naming convention "T[dim]_[level]"
        assert(varname[0]=="T")
        tdim = varname.split("_")[0]
        dim = tdim[1:]

        # If no constraints from microkernel on that dim: skip
        if dim not in microkernel.keys():
            continue

        size_microk = microkernel[dim]
        # If divisibility does not match, round down the value
        if (val % size_microk != 0):
            nval = int( math.floor(val / size_microk) * size_microk)
            # If nval==0 (too low), make it equal to size_microk
            if (nval<size_microk):
                nval = size_microk

            best_sol[varname] = nval
    return best_sol



# [Experimental - slow but works] Try to perform the integral solving cache level per cache level
def gen_and_solve_int_per_lvl(filename: str, is_filename=True, cache=None, nlevels=None,
        microk=None, b_integer_sol=False, b_solve=False, b_verbose=False):
    #assert(b_integer_sol==True)  # No longer the case
    assert(b_solve==True)


    # Initial problem building to get the info from the parsing
    (prog, microkernel, cache, _, param_values, _, _) = gen_problem_from_yaml(filename,
            is_filename=is_filename, microk=microk, nlevels=nlevels, b_integer_sol=b_integer_sol)
    assert(cache != None)

    # Adaptation experiment
    if adaptation_expe:
        cache = adapting_cache_sizes(cache)
        for l in range(min(len(cache),2)):
            param_values[f'C_{l+1}'] = cache[l]


    # Recall gen_problems, but on inner levels (and gradually consider the outermost ones)
    dprevsol = dict()

    # For each levels
    for klvl in range(0,len(cache)):
        # We do not care about data movement, but want to reduce the problem size
        b_no_data_vol = True
        pbs_lvl_k = gen_problems(prog, (klvl+1), None, microkernel, b_integer_sol, b_no_data_vol)

        
        # Preprocess pbs_lvl_k (add previous solution + problem param value propagation)

        # DEBUG
        #for pb in pbs_lvl_k:
        #    print(pb[1].build_str_ampl_problem(param_values))
        #    print("============================")
        pbs_lvl_k = preprocess_problem_int_per_lvl(pbs_lvl_k, dprevsol, param_values)
        # DEBUG
        #for pb in pbs_lvl_k:
        #    print(pb[1].build_str_ampl_problem(param_values))
        #    print("============================")


        # Solve it & get the best permutation/solution
        sol_k, best_perm_k = solve(pbs_lvl_k, param_values)
        best_sol = sol_k[ best_perm_k[1] ][0]
        
        # If we are in rational (experiment), then round to the nearest multiple of the microkernel
        if (not b_integer_sol) and (microkernel != None):
            best_sol = adapt_rat_to_int_per_lvl(best_sol, microkernel)

        
        # Get the new integral solution
        #print(best_sol)
        # Example of best_sol :
        # {'Tw_0': 1, 'Th_0': 1, 'Tf_0': 32, 'Tb_0': 1, 'Tc_0': 33, 'Tx_0': 9, 'Ty_0': 12}
        dprevsol |= best_sol

        # DEBUG: early interuption
        #if (klvl==2):
        #    return microkernel, cache, pbs_lvl_k, param_values, sol_k, best_perm_k

    # Wrapping up things
    return microkernel, cache, pbs_lvl_k, param_values, sol_k, best_perm_k


# Main entry function from the command line size
def gen_and_solve(filename: str, is_filename=True, cache=None, nlevels=None, microk=None,
        b_integer_sol=False, b_solve=False, b_verbose=False):
    # Create the problem (to be solved)
    _, microkernel, cache, pbs, param_values, _, _ = gen_problem_from_yaml(filename, is_filename=is_filename,
        cache=cache, nlevels=nlevels, microk=microk, b_integer_sol=b_integer_sol)

    # Solve it
    if b_solve:
        sol, best_perm = solve(pbs, param_values)
        if b_verbose:
            for i in range(len(pbs)):
                print(bold('Solution for permutation {i+1}'),pbs[i][0],':')
                print(tab(),bold('Footprint: '), pbs[i][1].g)
                print(tab(),bold('Analytical cost expression: '))
                print(reindent(pretty(pbs[i][1].f),2*TAB))
                print(reindent(bold('Solution:')),sol[i])
            print('\n')
    else:
        sol = None
        best_perm = None

    return microkernel, cache, pbs, param_values, sol, best_perm

# Auxilliary function - build the linearized access function
#   + the order of dims in this lin access func
# NOTE: ASSUME THAT THE ORDER IN prog.arrays IN THE YML ARE THE ALLOCATION ORDER
def build_maccess_func_coeff(mprog_arrays, param_values, cache_line_size):
    maccess_func_coeff = dict()
    ml_dim_name_stride = dict()
    for (arr_name, arr_info) in mprog_arrays.items():
        laccess = arr_info[0]  # "List[str]"

        # DEBUG
        #print(f"laccess = {laccess}")
        #print(f"param_values = {param_values}")
        #print(f"cache_line_size = {cache_line_size}")
        #print()

        l_dim_name_stride = []

        lacc_fun_coeff = []

        # Stride of innermost level is always 1 - assume vectorization
        lacc_fun_coeff.append(1)

        # Strides taken by "cacheset_estimation.py" are on cache line units
        current_stride = 1.0 / cache_line_size

        for k in range(len(laccess)-1, -1, -1):
            access = laccess[k]
            # access is a "str" that contains an affine expr (often a single symbol)
            # We also need to evaluate the symbol with the corresponding problem sizes

            # For each [a-zA-Z]* in the expr, substitutions with the param name ("N[dimname]")
            # => Split expression along "+", then assumption that no coeff
            #      (=> print asking for extension)
            # Then, rest of the string (with strip() ) is the name
            temp_l_dim_name_stride = []
            lterms = access.split("+")
            lval_temp = []
            for term in lterms:
                temp_dimname = term.strip()
                assert(not temp_dimname.isdecimal())
                # ^ If this assert triggers, we need to implement coeff management
                temp_l_dim_name_stride.append(temp_dimname)

                param_name = param_naming_conv(temp_dimname)

                # Substitution with the real value
                assert(param_name in param_values)

                lval_temp.append(param_values[param_name]-1)

            # Example: "(Nx-1) + (Ny-1) + (Nz-1) + 1", because constraint is 0 <= x < Nx
            val_access_k = 1
            for val_temp in lval_temp:
                val_access_k += val_temp

            # Update current_stride (because arrays are rectangular)
            current_stride = val_access_k * current_stride

            if not float(current_stride).is_integer():
                print(current_stride)
                print("FATAL ISSUE - stride after innermost dim are not multiple of cache line size")
                exit(1)
            current_stride = int(current_stride)

            # Duplicate the stride when several dim are in the same expr (ex: "x+w")
            for _ in lterms:
                lacc_fun_coeff.append(current_stride)

            temp_l_dim_name_stride.reverse()
            l_dim_name_stride = l_dim_name_stride + temp_l_dim_name_stride

        # Remove the last element
        lacc_fun_coeff = lacc_fun_coeff[0:len(lacc_fun_coeff)-1]

        lacc_fun_coeff.reverse()
        l_dim_name_stride.reverse()

        maccess_func_coeff[arr_name] = lacc_fun_coeff
        ml_dim_name_stride[arr_name] = l_dim_name_stride

    return maccess_func_coeff, ml_dim_name_stride


# Auxilliary function - compute penality accesses
def compute_penality_accesses(n_cache_sets, cache_line_size, maccess_func_coeff, ml_dim_name_stride, best_sol, ldims):
    # Compute the level depth of best_sol
    num_tile_var = len(list(best_sol.keys()))
    num_dim = len(ldims)
    assert(num_tile_var % num_dim == 0)
    lvl_best_sol = int(num_tile_var / num_dim) - 1

    # DEBUG
    #print(best_sol)
    #print(lvl_best_sol)

    lfp_penality = []
    for (arr_name, l_dim_name_stride) in ml_dim_name_stride.items():
        # Already built previously once for all
        access_func_coeff = maccess_func_coeff[arr_name]

        # Extract the tile sizes found (in best_sol) to get bound_tiles
        bound_tiles = []

        # l_dim_name_stride => Order of dims used for the strides of access_func_coeff
        for dim in l_dim_name_stride:
            tile_name = f'T{dim}_{lvl_best_sol}'
            tilesize = best_sol[tile_name]

            bound_tiles.append([0, tilesize])

        # Correct the very last bound_tiles:
        # (tile sizes taken by "cacheset_estimation.py" are on cache line units)
        bound_tiles[-1][1] = int(math.ceil(bound_tiles[-1][1] / cache_line_size))


        # DEBUG
        #print()
        #print(access_func_coeff)
        #print(l_dim_name_stride)
        #print(best_sol)
        #print(bound_tiles)


        # Call the algorithm and get the penality on the footprint
        _, frac_perc_usage = cacheset_estimation.main_algo_fun(
                access_func_coeff, n_cache_sets, bound_tiles)

        # percentage of usage of cache capacity = inverse of penality on footprint
        fp_penality = [ frac_perc_usage[1], frac_perc_usage[0] ]
        lfp_penality.append(fp_penality)

    return lfp_penality


# [Experiment] "gen_and_solve_int_per_lvl", but with an heuristic to estimate the
#   amount of cache really used. Do a first solving, then use the found tile
#   to call "cacheset_estimation::main_algo_fun" and have an estimation
def gen_and_solve_int_per_lvl_cacheset_estim(filename: str, is_filename=True, cache=None, nlevels=None,
        microk=None, b_integer_sol=False, b_solve=False, b_verbose=False):
    assert(b_solve==True)

    # Initial problem building to get the info from the parsing
    (prog, microkernel, cache, _, param_values, lcache_set, cache_line_size) = gen_problem_from_yaml(filename,
            is_filename=is_filename, microk=microk, nlevels=nlevels, b_integer_sol=b_integer_sol)

    assert(cache != None)
    assert(lcache_set != [])

    # Prepare for penality estimation algo: access_func_coeff
    maccess_func_coeff, ml_dim_name_stride = build_maccess_func_coeff(prog.arrays, param_values, cache_line_size)

    # DEBUG
    #print(f"maccess_func_coeff = {maccess_func_coeff}")
    #print(f"ml_dim_name_stride = {ml_dim_name_stride}")


    # Recall gen_problems, but on inner levels (and gradually consider the outermost ones)
    dprevsol = dict()

    # For each levels
    for klvl in range(0,len(cache)):
        # We do not care about data movement, but want to reduce the problem size
        b_no_data_vol = True
        pbs_lvl_k = gen_problems(prog, (klvl+1), None, microkernel, b_integer_sol, b_no_data_vol)

        # Preprocess pbs_lvl_k (add previous solution + problem param value propagation)
        pbs_lvl_k = preprocess_problem_int_per_lvl(pbs_lvl_k, dprevsol, param_values)

        # DEBUG
        #for pb in pbs_lvl_k:
        #    print(pb[1].build_str_ampl_problem(param_values))
        #    print("============================")

        # First pass of solving to get the best permutation/solution
        sol_k, best_perm_k = solve(pbs_lvl_k, param_values)
        best_sol = sol_k[ best_perm_k[1] ][0]

        # If we are in rational (experiment), then round to the nearest multiple of the microkernel
        if (not b_integer_sol) and (microkernel != None):
            best_sol = adapt_rat_to_int_per_lvl(best_sol, microkernel)
        
        # Get the penality per access and modify the problems
        n_cache_sets = lcache_set[klvl]
        lfp_penality = compute_penality_accesses(n_cache_sets, cache_line_size,
            maccess_func_coeff, ml_dim_name_stride, best_sol, prog.dims)

        # Note: lfp_penality is a fraction (encoded as a list of 2 elements)

        # DEBUG
        #print(f"lfp_penality = {lfp_penality}")



        # Use gen_problems with new penality list input
        n_pbs_lvl_k = gen_problems(prog, (klvl+1), None, microkernel,
            b_integer_sol, b_no_data_vol, lfp_penality)
        pbs_lvl_k = preprocess_problem_int_per_lvl(pbs_lvl_k, dprevsol, param_values)


        # Second pass of solving (with the penality added)
        sol_k, best_perm_k = solve(n_pbs_lvl_k, param_values)
        best_sol = sol_k[ best_perm_k[1] ][0]

        # If we are in rational (experiment), then round to the nearest multiple of the microkernel
        if (not b_integer_sol) and (microkernel != None):
            best_sol = adapt_rat_to_int_per_lvl(best_sol, microkernel)


        # Get the new integral solution
        #print(best_sol)
        # Example of best_sol :
        # {'Tw_0': 1, 'Th_0': 1, 'Tf_0': 32, 'Tb_0': 1, 'Tc_0': 33, 'Tx_0': 9, 'Ty_0': 12}
        dprevsol |= best_sol

        # DEBUG: early interuption
        #if (klvl==2):
        #    return microkernel, cache, pbs_lvl_k, param_values, sol_k, best_perm_k

    # Wrapping up things
    return microkernel, cache, pbs_lvl_k, param_values, sol_k, best_perm_k



# ================================================
# Ttile interface functions
# ================================================

# [Ttile interface] To get ONLY the permutation
def get_perm_from_microkernel_direct_yaml(yaml_content_dict, microkernel):
    _, microkernel, _, pbs, param_values, _, _ = gen_problem_from_yaml(yaml_content_dict, is_filename=False, microk=microkernel)
    sol, best_perm = solve(pbs, param_values)
    param_values.update(sol[best_perm[1]][0])
    return output_permonly(pbs[best_perm[1]][0], param_values, microkernel)

# [Ttile interface] Get the list of permutation
def get_lperms_from_microkernel_direct_yaml(yaml_content_dict, microkernel):
    # Note: actually does a bit more work than needed, but nothing heavy
    prog, microkernel, v_caches, pbs, _, _, _ = gen_problem_from_yaml(yaml_content_dict, is_filename=False, microk=microkernel)
    nlevels = len(v_caches)
    ll_ml_perm = gen_multi_level_permutation(prog, nlevels, microkernel)

    # Flatten the list to remove the level/level information
    #l_ml_perm_res = []
    #for l_ml_perm in ll_ml_perm:
    #    n_ml_perm_res = []
    #    for perm_fragment in l_ml_perm:
    #        n_ml_perm_res.extend(perm_fragment)
    #    l_ml_perm_res.append(n_ml_perm_res)

    # No convertion to a better format needed here
    #return l_ml_perm_res

    return ll_ml_perm

# ===========

# [Ttile interface] Get the permutation and the sizes, rational/integral solving
#       Integral solving does not scale above 1 cache level, due to solver limitations
def get_perm_sizes_from_microkernel_direct_yaml(yaml_content_dict, microkernel, b_integer_sol, b_all_perm):
    # Adaptation
    if adaptation_expe:
        lcachessizes = yaml_content_dict['values']['cache']
        nlcachessizes = adapting_cache_sizes(lcachessizes)
        yaml_content_dict['values']['cache'] = nlcachessizes


    _, microkernel, _, pbs, param_values, _, _ = gen_problem_from_yaml(yaml_content_dict, is_filename=False,
                microk=microkernel, b_integer_sol=b_integer_sol)
    sol, best_perm = solve(pbs, param_values)

    # Get only infos about the permutation and the sizes from the solution
    if b_all_perm:
        loutput_permsizes = []
        for k in range(len(pbs)):
            curr_pb = pbs[k][0]
            curr_sol = sol[k][0]
            nelem = output_permsizes(curr_pb, param_values, curr_sol, microkernel)
            loutput_permsizes.append(nelem)
        return unique_schemes(loutput_permsizes)
    else:
        # Only the best one
        best_pb = pbs[best_perm[1]][0]
        best_sol = sol[best_perm[1]][0]
        return [output_permsizes(best_pb, param_values, best_sol, microkernel)]


# [Ttile interface] Get the permutation and the sizes, while asking for a divisible solution
# Note: signature must be the same than "get_perm_sizes_from_microkernel_direct_yaml"
#      Experiment was not conclusive
def get_perm_sizes_from_microkernel_divisible_direct_yaml_rat_to_int(yaml_content_dict,
            microkernel, b_integer_sol, b_all_perm):
    assert(b_integer_sol)
    microkernel, _, pbs, param_values, sol, best_perm = gen_and_solve_rat_to_int(
        yaml_content_dict, is_filename=False, microk=microkernel, b_integer_sol=True,
        b_solve=True, b_verbose=False)

    # Get only infos about the permutation and the sizes from the solution
    if b_all_perm:
        loutput_permsizes = []
        for k in range(len(pbs)):
            curr_pb = pbs[k][0]
            curr_sol = sol[k][0]
            nelem = output_permsizes(curr_pb, param_values, curr_sol, microkernel)
            loutput_permsizes.append(nelem)
        return unique_schemes(loutput_permsizes)
    else:
        # Only the best one
        best_pb = pbs[best_perm[1]][0]
        best_sol = sol[best_perm[1]][0]
        return [output_permsizes(best_pb, param_values, best_sol, microkernel)]


# [Ttile interface] Get the permutation and the sizes, while asking for a divisible solution
# Note: signature must be the same than "get_perm_sizes_from_microkernel_direct_yaml"
def get_perm_sizes_from_microkernel_divisible_direct_yaml_per_lvl(yaml_content_dict,
            microkernel, b_integer_sol, b_all_perm, b_cacheset_estim):

    if b_cacheset_estim:
        microkernel, _, pbs, param_values, sol, best_perm = gen_and_solve_int_per_lvl_cacheset_estim(
            yaml_content_dict, is_filename=False, microk=microkernel, b_integer_sol=b_integer_sol,
            b_solve=True, b_verbose=False) 
    else:
        microkernel, _, pbs, param_values, sol, best_perm = gen_and_solve_int_per_lvl(
            yaml_content_dict, is_filename=False, microk=microkernel, b_integer_sol=b_integer_sol,
            b_solve=True, b_verbose=False)


    # Get only infos about the permutation and the sizes from the solution
    if b_all_perm:
        loutput_permsizes = []
        for k in range(len(pbs)):
            curr_pb = pbs[k][0]
            curr_sol = sol[k][0]
            nelem = output_permsizes(curr_pb, param_values, curr_sol, microkernel)
            loutput_permsizes.append(nelem)
        return unique_schemes(loutput_permsizes)
    else:
        # Only the best one
        best_pb = pbs[best_perm[1]][0]
        best_sol = sol[best_perm[1]][0]
        return [output_permsizes(best_pb, param_values, best_sol, microkernel)]




# ================================================
# Command line interface functions
# ================================================

class keyvalue(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, dict())
        for value in values:
            key, value = value.split('=')
            getattr(namespace, self.dest)[key] = int(value)

def main():
# Option parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('-c', '--cost', help='IO cost expression for each permutation and each cache level',action='store_true')
    parser.add_argument('-s', '--solve', help='Best permutation and tiles size that minimizes IO cost', action='store_true')
    parser.add_argument('--cache', help='cache sizes in words from inner to outer', metavar='S', type=int, nargs='+')
    parser.add_argument('--depth', help='cache depth', type=int)
    parser.add_argument('--microkernel', help='microkernel', nargs='*', action=keyvalue)
    parser.add_argument('--schedule', help='Schedule of best solution (required -s)', action='store_true')
    parser.add_argument('-o', '--output')
    parser.add_argument('--output-perm', help='Output permutation only in TTile format', action='store_true')
    parser.add_argument('--textual_output', help='Print output as plain text', action='store_true')
    parser.add_argument('--symbolic', action='store_true')
    parser.add_argument('-i', '--int_solv', help='[Might explode] Ask the solver to find an integral solution', action='store_true')
    parser.add_argument('-ri', '--rat_int_solv', help='[Experimental] Use a rational solution to help finding an integral solution', action='store_true')
    parser.add_argument('-ilvl', '--int_per_lvl', help='[Experimental] Solve the integral problem cache level per cache level', action='store_true')
    parser.add_argument('-rlvl', '--rat_per_lvl', help='[Experimental] Solve the rational problem cache level per cache level', action='store_true')
    parser.add_argument('-cs', '--cacheset', help='[Experimental] Estimate the penalty due to non-fully associative cache, requires -ilvl or -rlvl.', action='store_true')
    args = parser.parse_args()
    cfg.verbose = args.verbose

    if args.debug: __debug=True
    
    # Main function doing everything
    if (args.rat_int_solv):
        args.int_solv = True
        args.solve = True
        microkernel, cache, pbs, param_values, sol, best_perm = gen_and_solve_rat_to_int(
            args.input, is_filename=True, cache=args.cache,
            nlevels=args.depth, microk=args.microkernel, b_integer_sol=args.int_solv,
            b_solve=args.solve, b_verbose=args.verbose)
    elif (args.int_per_lvl or args.rat_per_lvl) and args.cacheset:
        args.int_solv = args.int_per_lvl  # True if args.int_per_lvl , else False
        args.solve = True
        microkernel, cache, pbs, param_values, sol, best_perm = gen_and_solve_int_per_lvl_cacheset_estim(
            args.input, is_filename=True, cache=args.cache,
            nlevels=args.depth, microk=args.microkernel, b_integer_sol=args.int_solv,
            b_solve=args.solve, b_verbose=args.verbose)
    elif (args.int_per_lvl or args.rat_per_lvl):
        args.int_solv = args.int_per_lvl  # True if args.int_per_lvl , else False
        args.solve = True
        microkernel, cache, pbs, param_values, sol, best_perm = gen_and_solve_int_per_lvl(
            args.input, is_filename=True, cache=args.cache,
            nlevels=args.depth, microk=args.microkernel, b_integer_sol=args.int_solv,
            b_solve=args.solve, b_verbose=args.verbose)
    else:
        microkernel, cache, pbs, param_values, sol, best_perm = gen_and_solve(
            args.input, is_filename=True, cache=args.cache,
            nlevels=args.depth, microk=args.microkernel, b_integer_sol=args.int_solv,
            b_solve=args.solve, b_verbose=args.verbose)

    #_, microkernel, cache, pbs, param_values, _, _ = gen_problem_from_yaml(args.input, cache=args.cache,
    #    nlevels=args.depth, microk=args.microkernel, b_integer_sol=args.int_solv)
    #if args.solve:
    #    sol, best_perm = solve(pbs, param_values)
    #    if args.verbose:
    #        for i in range(len(pbs)):
    #            print(bold('Solution for permutation {i+1}'),pbs[i][0],':')
    #            print(tab(),bold('Footprint: '), pbs[i][1].g)
    #            print(tab(),bold('Analytical cost expression: '))
    #            print(reindent(pretty(pbs[i][1].f),2*TAB))
    #            print(reindent(bold('Solution:')),sol[i])
    #        print('\n')

    print('\n'*3,bold('-------------'))
    if args.cost:
        if args.textual_output:
            print('IO cost expressions:')
            for i in range(len(pbs)):
                print(f'\tPermutation {i+1}: ', '  '.join(["L1:..."]+[f'L{d+2}:{p}' for d,p in enumerate(pbs[i][0])]))
                print('\tFootprint: ', pbs[i][1].g)
                print('\t\t', pbs[i][1].f,'\n')
        else:
            print(bold('IO cost expressions:'))
            for i in range(len(pbs)):
                print(tab(),bold(f'Permutation {i+1}: '), '  '.join(["L1:..."]+[f'L{d+2}:{p}' for d,p in enumerate(pbs[i][0])]))
                print(tab(),bold('Footprint: '), pretty(pbs[i][1].g))
                print(reindent(pretty(pbs[i][1].f),2*TAB),'\n')
    
    if args.solve:
        if args.textual_output:
            print('Cache sizes (in words): ', cache)
            print('Best solution:')
            print('\tPermutation: ', pretty_permutation(pbs[best_perm[1]][0], sol[best_perm[1]][0], microkernel))
            print('\tFootprint: ', pbs[best_perm[1]][1].expressions['footprint'])
            print('\tAnalytical cost expression: ')
            print('\t\t', pbs[best_perm[1]][1].expressions['cost'])
            print('\t\t', pbs[best_perm[1]][1].f)
            print('\tOptimal sizes: ', '   '.join(str(parse_expr(variable))+f'={value}' for i, (variable,value) in enumerate(sol[best_perm[1]][0].items())))
            print('\tParameters: ', '   '.join(str(parse_expr(param, local_dict={'Ne': Symbol('Ne')}))+f'={value}' for i, (param,value) in enumerate(param_values.items())))
            print('\tTotal Cost: ', int(sol[best_perm[1]][1]))
            param_values.update(sol[best_perm[1]][0])
            print('\tData volume: ', '    '.join(f'{k}: {int(v.evalf(subs= param_values))}' for k, v in pbs[best_perm[1]][1].expressions['cost'].items()))
            print('\tData footprint: ', '    '.join(f'{k}: {int(v.evalf(subs= param_values))}' for k, v in pbs[best_perm[1]][1].expressions['footprint'].items()))
            if args.schedule:
                print('\tSchedule: ')
                pretty_schedule(pbs[best_perm[1]][0], sol[best_perm[1]][0], param_values, microkernel, '\t\t')
        else:
            print(bold('Cache sizes (in words): '), cache)
            print(bold('Best solution:'))
            print(tab(),bold(f'Permutation: '), pretty_permutation(pbs[best_perm[1]][0], sol[best_perm[1]][0], microkernel))
            print(tab(),bold('Footprint: '), pretty(pbs[best_perm[1]][1].expressions['footprint']))
            print(tab(),bold('Analytical cost expression: '))
            print(reindent(pretty(pbs[best_perm[1]][1].expressions['cost']),2*TAB))
            print(reindent(pretty(pbs[best_perm[1]][1].f),2*TAB))
            print(tab(), bold('Optimal sizes: '), '   '.join(pretty(parse_expr(variable))+f'={value}' for i, (variable,value) in enumerate(sol[best_perm[1]][0].items())))
            print(tab(), bold('Parameters: '), '   '.join(pretty(parse_expr(param, local_dict={'Ne': Symbol('Ne')}))+f'={value}' for i, (param,value) in enumerate(param_values.items())))
            print(tab(), bold('Total Cost: '), int(sol[best_perm[1]][1]))
            param_values.update(sol[best_perm[1]][0])
            print(tab(), bold('Data volume: '), '    '.join(f'{k}: {int(v.evalf(subs= param_values))}' for k, v in pbs[best_perm[1]][1].expressions['cost'].items()))
            print(tab(), bold('Data footprint: '), '    '.join(f'{k}: {int(v.evalf(subs= param_values))}' for k, v in pbs[best_perm[1]][1].expressions['footprint'].items()))
            if args.schedule:
                print(tab(), bold('Schedule: '))
                pretty_schedule(pbs[best_perm[1]][0], sol[best_perm[1]][0], param_values, microkernel, tab()+tab())


        if args.output:
            with open(args.output, 'w') as fout:
                fout.write(output_nico(pbs[best_perm[1]][0], param_values, microkernel))

        if args.output_perm:
            print(output_permonly(pbs[best_perm[1]][0], param_values, microkernel))

        if args.symbolic:
            print(bold('\n Symbolic bound:'))
            symbolic_bounds.output(args.input, pbs[best_perm[1]][1].f, pbs[best_perm[1]][1].g[0], sol[best_perm[1]][0])
    
if __name__ == '__main__':
    main()

from sympy import *
import sys
import yaml

def output(filename, ssol, scond, sol):
    val = {}
    exp = {}
    is_tc=False
    with open(filename) as fprog :
        y = yaml.load(fprog.read(), Loader=yaml.SafeLoader)
        dims = y['dims']
        for i,d in enumerate(dims):
            val[f'N_{d}'] = y['values']['dims'][i]
        if 'A' in y['arrays']: # is tensor
            is_tc=True
            for A, r in y['reuse'].items():
                for dr in r:
                    exp[dr] = len(r)
        else:
            for d in dims:
                exp[d] = 1
        val.update(sol)

    local_dict = {}
    Td, Nd = {}, {}
    T = Symbol('T', positive=True)


    for d in dims:
        Nd[d] = Symbol(str.upper(d), positive=True)
        Td[d] = T**Rational(1, exp[d])
        if val[f'T{d}_0'] == 1:
            Td[d] = 1
        elif not is_tc and val[f'T{d}_0'] == val[f'N_{d}']:
            Td[d] = Nd[d]

        local_dict[f'N_{d}'] = Nd[d]
        local_dict[f'T{d}_0'] = Td[d]

    #print(local_dict)
    sol = ssol.subs(local_dict)
    cond = scond.subs(local_dict)

    #print(local_dict)
    #pprint(sol)
    #pprint(cond)

    S = Symbol('S')

    x=solveset(cond - S, T)
    for xx in x:
        pprint((sol.subs(T, xx)))
        break
        #print(latex((sol.subs(T, xx))))
        #print(latex(factor(xx)))
        #print(latex(sol))
